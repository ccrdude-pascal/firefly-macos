program FireflyMacCLIBExampleConsole;

{$mode Delphi}{$H+}

{$linklib libc.dylib}
{$linklib c}

uses
   {$IFDEF UNIX}
   cthreads,
   {$ENDIF}
   DynLibs,
   SysUtils,
   Classes,
   BaseUnix;

const
   clib = 'c';

type
   PCLibFile = cint64;
   Tfopen = function(__filename: pansichar; __modes: pansichar): PCLibFile; cdecl;
   Tfwrite = function(__ptr: Pointer; __size: size_t; __n: size_t; __s: PCLibFile): size_t; cdecl;

   {
     The fopen() function opens the file whose name is the string pointed to
     by path and associates a stream with it.

     FILE * fopen(const char * restrict path, const char * restrict mode);

     @see https://man.freebsd.org/cgi/man.cgi?query=fopen
   }
   function fopen(__filename: pansichar; __modes: pansichar): PCLibFile; cdecl; external clib Name 'fopen';
   {
     The function fread() reads nmemb objects, each size  bytes  long, from
     the  stream pointed to by stream, storing them at the location given by ptr.

     size_t fread(void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);

     @see https://man.freebsd.org/cgi/man.cgi?query=fread
   }
   function fread(__ptr: pointer; __size: size_t; __n: size_t; __stream: PCLibFile): size_t; cdecl; external clib Name 'fread';

   function fwrite(__ptr: pointer; __size: size_t; __n: size_t; __s: PCLibFile): size_t; cdecl; external clib Name 'fwrite';
   function fseek(__stream: PCLibFile; __off: longint; __whence: longint): longint; cdecl; external clib Name 'fseek';
   function fclose(__stream: PCLibFile): longint; cdecl; external clib Name 'fclose';
   function ftell(__stream: PCLibFile): longint; cdecl; external clib Name 'ftell';
   procedure rewind(__stream: PCLibFile); cdecl; external clib Name 'rewind';
   function fgetc(__stream:PCLibFile):longint;cdecl;external clib name 'fgetc';
   function feof(__stream:PCLibFile):longint;cdecl;external clib name 'feof';

   procedure LoadLibraryTest;
   var
      h: TLibHandle = 0;
      fopenp: Tfopen;
      fwritep: Tfwrite;
      cTest: pansichar = 'Hello FreePascal';
      hFile: PCLibFile = 0;
      iBytesWritten: integer = 0;
   begin
      h := LoadLibrary('/usr/local/Cellar/clib/2.8.5/bin/clib');
      try
         if h <> 0 then begin
            WriteLn('[+] LoadLibrary');
         end else begin
            WriteLn('[-] LoadLibrary');
            Exit;
         end;
         fopenp := Tfopen(GetProcAddress(h, 'fopen'));
         if Assigned(fopenp) then begin
            WriteLn('[+] GetProcAddress(fopen)');
         end else begin
            WriteLn('[-] GetProcAddress(fopen)');
            Exit;
         end;
         fwritep := Tfwrite(GetProcAddress(h, 'fwrite'));
         if Assigned(fwritep) then begin
            WriteLn('[+] GetProcAddress(fwrite)');
         end else begin
            WriteLn('[-] GetProcAddress(fwrite)');
            Exit;
         end;
         hFile := fopenp('/Users/patrick/test-gibbetnicht.txt', 'r+');
         if hFile <> 0 then begin
            WriteLn('[-] fopen (missing file found?)');
            Exit;
         end else begin
            WriteLn('[+] fopen (missing file, no handle)');
         end;
         hFile := fopenp('/Users/patrick/test-temp.txt', 'w+b');
         if hFile <> 0 then begin
            WriteLn('[+] fopen (write test file)');
         end else begin
            WriteLn('[-] fopen (write test file)');
            Exit;
         end;

         iBytesWritten := fwrite(cTest, Length(cTest), 1, hFile);
         if (iBytesWritten <> 0) then begin
            WriteLn('[+] fwrite (write test file), ', iBytesWritten, ' records.');
         end else begin
            WriteLn('[-] fwrite (write test file)');
            Exit;
         end;


      finally
         WriteLn;
      end;
   end;

   procedure SimpleFileMissingTest;
   var
      h: PCLibFile = 0;
   begin
      h := fopen('/Users/patrick/test.txt', 'r');
      if h <> 0 then begin
         WriteLn('[-] fopen (missing file found?)');
      end else begin
         WriteLn('[+] fopen (missing file, no handle)');
      end;
   end;

   procedure SimpleWriteTest;
   var
      h: PCLibFile = 0;
      cTest: pansichar = 'Hello FreePascal';
      iBytesWritten: integer;
   begin
      h := fopen('/Users/patrick/test-temp2.txt', 'w+');
      if h <> 0 then begin
         WriteLn('[+] fopen (write test file)');
      end else begin
         WriteLn('[-] fopen (write test file)');
         Exit;
      end;
      try
         iBytesWritten := fwrite(cTest, Length(cTest), 1, h);
         if (iBytesWritten <> 0) then begin
            WriteLn('[+] fwrite (write test file), ', iBytesWritten, ' records.');
         end else begin
            WriteLn('[-] fwrite (write test file)');
            Exit;
         end;
      finally
         WriteLn;
         fclose(h);
      end;
   end;

   procedure SimpleOpenTest;
   var
      h: PCLibFile = 0;
      aBuffer: array[0..127] of ansichar;
      p: pansichar = nil;
      iBytesRead: integer;
      i: integer;
   begin
      h := fopen('/Users/patrick/test.txt', 'r');
      if h <> 0 then begin
         WriteLn('[+] fopen (read test file)');
      end else begin
         WriteLn('[-] fopen (read test file)');
         Exit;
      end;
      GetMem(p, 128);
      try
         rewind(h);
         (*
         iBytesRead := fread(p, 15, 1, h);
         if (iBytesRead <> 0) then begin
            WriteLn('[+] fread (test file), ', iBytesRead, ' records: ', p);
         end else begin
            WriteLn('[-] fread (test file)');
            Exit;
         end;
         *)
         while feof(h)=0 do begin
            i := fgetc(h);
            //Write(IntToHex(i, 4) + ' ');
            Write(WideChar(i));
         end;
      finally
         WriteLn;
         FreeMem(p);
      end;
   end;

begin
   LoadLibraryTest;
   SimpleFileMissingTest;
   SimpleWriteTest;
   SimpleOpenTest;
   WriteLn('Press ENTER to finish.');
   ReadLn;
end.
