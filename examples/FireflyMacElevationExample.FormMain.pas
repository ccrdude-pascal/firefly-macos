unit FireflyMacElevationExample.FormMain;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   Firefly.MacOS.Elevation;

type

   { TForm1 }

   TForm1 = class(TForm)
      bnExecute: TButton;
      editCommand: TEdit;
      memoOutput: TMemo;
      memoParameters: TMemo;
      procedure bnExecuteClick(Sender: TObject);
   private

   public

   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

uses
   Process;

   { TForm1 }

procedure TForm1.bnExecuteClick(Sender: TObject);
var
   sOutput: string;
   sCommand: string;
   a: array of TProcessString;
   i: integer;
begin
   SetLength(a, memoParameters.Lines.Count);
   for i := 0 to Pred(memoParameters.Lines.Count) do begin
      a[i] := memoParameters.Lines[i];
   end;
   sCommand := editCommand.Text;
   TMacOSElevation.Instance.RunCommand(sCommand, a, sOutput);
   memoOutput.Lines.Text := sOutput;
end;

end.
