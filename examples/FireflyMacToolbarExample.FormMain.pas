{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Main form for macOS toolbar example.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-02-07  pk  5m  Added header
// *****************************************************************************
   )
}
unit FireflyMacToolbarExample.FormMain;

{$mode Delphi}{$H+}
{$modeswitch objectivec1}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ActnList,
   StdCtrls,
   Menus,
   CheckLst,
   ExtCtrls,
   CocoaAll,
   Firefly.MacOS.App,
   Firefly.MacOS.Toolbar,
   Firefly.MacOS.Toolbar.Items,
   Firefly.MacOS.Toolbar.EditorForm,
   FireflyMacToolbarExample.FormWithLCLToolbar;

type

   { TFormMacOSToolbarExample }

   TFormMacOSToolbarExample = class(TForm)
      Action1: TAction;
      Action2: TAction;
      Action3: TAction;
      Action4: TAction;
      Action5: TAction;
      aShowLCLToolbar: TAction;
      ActionList1: TActionList;
      CheckListBox1: TCheckListBox;
      GroupBox1: TGroupBox;
      ImageList1: TImageList;
      MainMenu1: TMainMenu;
      Memo1: TMemo;
      MenuItem1: TMenuItem;
      MenuItem2: TMenuItem;
      MenuItem3: TMenuItem;
      MenuItem4: TMenuItem;
      MenuItem5: TMenuItem;
      MenuItem6: TMenuItem;
      MenuItem7: TMenuItem;
      MenuItem8: TMenuItem;
      PopupMenu1: TPopupMenu;
      procedure Action1Execute({%H-}Sender: TObject);
      procedure Action2Execute({%H-}Sender: TObject);
      procedure Action3Execute({%H-}Sender: TObject);
      procedure Action4Execute({%H-}Sender: TObject);
      procedure Action5Execute({%H-}Sender: TObject);
      procedure aShowLCLToolbarExecute({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
   private
      FToolbar: TMacToolbar;
      FApp: TMacOSAppHelper;
      FGroup: TMacToolbarItem;
      FGroupItem1: TMacToolbarItem;
      FGroupItem2: TMacToolbarItem;
      FMenuItem: TMacToolbarMenuItem;
      FShareItem: TMacToolbarShareItem;
      FSearchItem: TMacToolbarSearchItem;
      procedure AddToolbar;
      procedure AddDock;
      procedure DoGetSharedStrings(AList: TStrings);
      procedure DoBeginSearch({%H-}Sender: TObject);
      procedure DoEndSearch({%H-}Sender: TObject);
      procedure DoSearchTextChange({%H-}Sender: TObject);
   public

   end;

var
   FormMacOSToolbarExample: TFormMacOSToolbarExample;

implementation

{$R *.lfm}

{ TFormMacOSToolbarExample }

procedure TFormMacOSToolbarExample.FormShow(Sender: TObject);
begin
   AddDock;
   AddToolbar;
end;

procedure TFormMacOSToolbarExample.Action1Execute(Sender: TObject);
begin
   Memo1.Lines.Add('Action 1 clicked');
   EditMacOSToolbarItems(FToolbar.Items);
end;

procedure TFormMacOSToolbarExample.Action2Execute(Sender: TObject);
begin
   Memo1.Lines.Add('Action 2 clicked');
end;

procedure TFormMacOSToolbarExample.Action3Execute(Sender: TObject);
begin
   Memo1.Lines.Add('Action 3 clicked');
end;

procedure TFormMacOSToolbarExample.Action4Execute(Sender: TObject);
begin
   Memo1.Lines.Add('Action 4 clicked');
end;

procedure TFormMacOSToolbarExample.Action5Execute(Sender: TObject);
begin
   Memo1.Lines.Add('Action 5 clicked');
end;

procedure TFormMacOSToolbarExample.aShowLCLToolbarExecute(Sender: TObject);
begin
   FormLCLToolbar.ShowModal;
end;


procedure TFormMacOSToolbarExample.AddToolbar;
var
   item: TMacToolbarItem;
begin
   FToolbar := TMacToolbar.Create(Self);
   FToolbar.DisplayMode := mtdmIconOnly;
   FToolbar.ImageList := ImageList1;

   item := FToolbar.Items.AddItem;
   item.Identifier := aShowLCLToolbar.Name;
   item.Action := aShowLCLToolbar;
   item.ImageDefaulType := NSImageNameBookmarksTemplate;
   item.IsImmovable := True;

   FGroup := FToolbar.Items.AddItemGroup('pageToolbarItemGroup1');

   item := FGroup.SubItems.AddItem;
   item.Identifier := 'addToolbarItem';
   item.Action := Action1;
   item.ImageDefaulType := NSImageNameAddTemplate;
   item.IsImmovable := True;
   item.IsCentered := True;

   item := FGroup.SubItems.AddItem('subsctractToolbarItem', 'Minus', 'Action 2', Action2);
   item.ImageDefaulType := NSImageNameRemoveTemplate;
   item.IsCentered := True;

   item := FToolbar.Items.AddItem;
   item.Identifier := 'dropDownToolbarItem';
   item.Action := Action3;
   item.ImageDefaulType := NSImageNameRefreshTemplate;
   item.IsCentered := True;
   item.IsSelectable := True;

   FGroup := FToolbar.Items.AddItemGroup('pageToolbarItemGroup2');

   FGroupItem1 := FGroup.SubItems.AddItem('vpnToolbarItem', Action4);
   FGroupItem1.ImageDefaulType := NSImageNameUser;
   FGroupItem1.IsImmovable := True;
   FGroupItem1.IsSelectable := True;

   FGroupItem2 := FGroup.SubItems.AddItem('mountsToolbarItem', Action5);
   FGroupItem2.ImageDefaulType := NSImageNameUserGuest;
   FGroupItem2.IsImmovable := True;
   FGroupItem2.IsSelectable := True;

   FMenuItem := FToolbar.Items.AddMenuItem;
   FMenuItem.ImageDefaulType := NSImageNameComputer;
   FMenuItem.Identifier := 'menuButtonItem';
   FMenuItem.Caption := 'Button w/ menu';
   FMenuItem.DropDownMenu := PopupMenu1;

   FSearchItem := FToolbar.Items.AddSearchItem;
   FSearchItem.Identifier := 'searchItem';
   FSearchItem.Hint := 'Search demo...';
   FSearchItem.Caption := 'Search demo';
   FSearchItem.OnBeginSearch := DoBeginSearch;
   FSearchItem.OnEndSearch := DoEndSearch;
   FSearchItem.OnChange := DoSearchTextChange;

   FShareItem := TMacToolbarShareItem(FToolbar.Items.AddItem(TMacToolbarShareItem));
   FShareItem.Caption := 'Give it away';
   FShareItem.OnGetSharedStrings := DoGetSharedStrings;

   EditMacOSToolbarItems(FToolbar.Items);

   FToolbar.Attach;

   FToolbar.SelectedItem := FGroupItem2;
end;

procedure TFormMacOSToolbarExample.AddDock;
begin
   FApp := TMacOSAppHelper.Create(Self);
   FApp.MenuItems := PopupMenu1;
   FApp.Attach;
end;

procedure TFormMacOSToolbarExample.DoGetSharedStrings(AList: TStrings);
begin
   AList.Assign(Memo1.Lines);
end;

procedure TFormMacOSToolbarExample.DoBeginSearch(Sender: TObject);
begin
   Memo1.Lines.Add('Search begins...');
end;

procedure TFormMacOSToolbarExample.DoEndSearch(Sender: TObject);
begin
   Memo1.Lines.Add('Search end...');
end;

procedure TFormMacOSToolbarExample.DoSearchTextChange(Sender: TObject);
begin
   Memo1.Lines.Add('Search text: ' + FSearchItem.SearchText);
end;

end.
