{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(LCL TToolbar import demo form for macOS toolbar.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-02-07  pk  5m  Added header
// *****************************************************************************
   )
}
unit FireflyMacToolbarExample.FormWithLCLToolbar;

{$mode ObjFPC}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ButtonPanel, ComCtrls,
   ActnList, Menus, StdCtrls,
   Firefly.MacOS.Toolbar,
   Firefly.MacOS.Toolbar.LCLImporter;

type

   { TFormLCLToolbar }

   TFormLCLToolbar = class(TForm)
      aButton1: TAction;
      aButton2: TAction;
      aDropdown1: TAction;
      alLCLToolbar: TActionList;
      ButtonPanel1: TButtonPanel;
      ImageList1: TImageList;
      Memo1: TMemo;
      MenuItem1: TMenuItem;
      PopupMenu1: TPopupMenu;
      ToolBar1: TToolBar;
      ToolButton1: TToolButton;
      ToolButton2: TToolButton;
      ToolButton3: TToolButton;
      procedure aButton1Execute({%H-}Sender: TObject);
      procedure aButton2Execute({%H-}Sender: TObject);
      procedure aDropdown1Execute({%H-}Sender: TObject);
      procedure FormCreate({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
   private
      FMacToolbar: TMacToolbarFromLCLToolbar;
   public

   end;

var
   FormLCLToolbar: TFormLCLToolbar;

implementation

{$R *.lfm}

{ TFormLCLToolbar }

procedure TFormLCLToolbar.aButton1Execute(Sender: TObject);
begin
   memo1.Lines.Add('aButton1 clicked');
end;

procedure TFormLCLToolbar.aButton2Execute(Sender: TObject);
begin
   memo1.Lines.Add('aButton2 clicked');
end;

procedure TFormLCLToolbar.aDropdown1Execute(Sender: TObject);
begin
   memo1.Lines.Add('aDropdown1 clicked');
end;

procedure TFormLCLToolbar.FormCreate(Sender: TObject);
begin
end;

procedure TFormLCLToolbar.FormShow(Sender: TObject);
begin
   FMacToolbar := TMacToolbarFromLCLToolbar.Create(Self);
end;

end.
