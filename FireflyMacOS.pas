{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyMacOS;

{$warn 5023 off : no warning about unused units}
interface

uses
   Firefly.MacOS.App, Firefly.MacOS.KeyChain, Firefly.MacOS.Elevation, 
   Firefly.MacOS.ClassHelpers, Firefly.MacOS.Toolbar.Headers, 
   Firefly.MacOS.Toolbar.Items, Firefly.MacOS.Toolbar.LCLImporter, 
   Firefly.MacOS.Toolbar, Firefly.MacOS.Toolbar.EditorForm, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyMacOS', @Register);
end.
