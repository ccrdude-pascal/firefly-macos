{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(List and control macOS VPN connections.)
   @see https://dev.to/andreassiegel/connect-to-a-vpn-from-the-command-line-on-mac-os-1e26

   @preformatted(
// *****************************************************************************
// Copyright: © 2023-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-24  pk   5m  Added header.
// *****************************************************************************
   )
}
unit Firefly.MacOS.VPN;

{$mode Delphi}{$H+}
{$modeswitch TypeHelpers}

interface

uses
   Classes,
   SysUtils,
   Dialogs,
   OVM.Attributes,
   OVM.ListView.Attributes,
   Generics.Collections;

type
   TVPNConnectionStatus = (vpncsUnknown, vpncsConnected, vpncsConnecting,
      vpncsDisconnected);

   TConnectionNameArray = array of string;

   { TVPNConnectionStatusHelper }

   TVPNConnectionStatusHelper = type helper for TVPNConnectionStatus
      function ToNetworkSetupText: string;
      procedure FromNetworkSetupText(AText: string);
   end;

   { TMacOSVPNConnection }

   TMacOSVPNConnection = class
   private
      FConnectionName: string;
   public
      function GetStatusText: string;
      function GetStatus: TVPNConnectionStatus;
      procedure Connect(Sender: TObject);
      procedure Disconnect(Sender: TObject);
      property ConnectionName: string read FConnectionName write FConnectionName;
   end;

   { TMacOSVPNConnections }

   TMacOSVPNConnections = class(TObjectList<TMacOSVPNConnection>)
   public
      procedure ReadFromSystem;
   end;


implementation

uses
   Process,
   RegExpr;

{ TMacOSVPNConnections }

procedure TMacOSVPNConnections.ReadFromSystem;
var
   sOut: string;
   r: TRegExpr;
   conn: TMacOSVPNConnection;
begin
   if RunCommand('scutil', ['--nc', 'list'], sOut) then
   begin
      r := TRegExpr.Create(
         '\* \(([^\\)]*)\)   ([0-9A-F\-]*) ([^\"]*)"([^\"]*)"[^\\[]*\s*\[([\S]*)\]');
      if r.Exec(sOut) then repeat
            conn := TMacOSVPNConnection.Create;
            conn.ConnectionName := r.Match[4];
            Self.Add(conn);
         until not r.ExecNext;
   end;
end;

{ TVPNConnectionStatusHelper }

function TVPNConnectionStatusHelper.ToNetworkSetupText: string;
begin
   case Self of
      vpncsUnknown: Result := '?';
      vpncsConnected: Result := 'connected';
      vpncsConnecting: Result := 'connecting';
      vpncsDisconnected: Result := 'disconnected';
   end;
end;

procedure TVPNConnectionStatusHelper.FromNetworkSetupText(AText: string);
var
   i: TVPNConnectionStatus;
begin
   for i in TVPNConnectionStatus do
   begin
      if SameText(AText, i.ToNetworkSetupText) then
      begin
         Self := i;
         Exit;
      end;
   end;
   Self := vpncsUnknown;
end;

{ TMacOSVPNConnection }

function TMacOSVPNConnection.GetStatusText: string;
var
   sOut: string;
begin
   Result := 'unknown';
   if RunCommand('networksetup', ['-showpppoestatus', Self.ConnectionName], sOut) then
   begin
      Result := Trim(sOut);
   end;
end;

function TMacOSVPNConnection.GetStatus: TVPNConnectionStatus;
begin
   Result.FromNetworkSetupText(GetStatusText);
end;

procedure TMacOSVPNConnection.Connect(Sender: TObject);
var
   sOut: string;
begin
   RunCommand('networksetup', ['-connectpppoeservice', Self.ConnectionName], sOut);
end;

procedure TMacOSVPNConnection.Disconnect(Sender: TObject);
var
   sOut: string;
begin
   RunCommand('networksetup', ['-disconnectpppoeservice', Self.ConnectionName], sOut);
end;

end.
