{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Code to elevate calls on macOS.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-24  pk   5m  Added header.
// *****************************************************************************
   )
}
unit Firefly.MacOS.Elevation;

{$mode Delphi}{$H+}
// if macOS
{$linkframework Security}
{$modeswitch objectivec1 on}
{$linklib c}

interface

uses
   Classes,
   SysUtils,
   MacOsAll,
   CocoaAll,
   Nullable,
   Process,
   Forms;

type

   { TMacOSElevation }

   TMacOSElevation = class
   protected
   class var FInstance: TMacOSElevation;
   protected
      FAuthenticationReference: AuthorizationRef;
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TMacOSElevation;
   public
      constructor Create;
      destructor Destroy; override;
      procedure RunCommand(AnExecutable: string; const AParameters: array of TProcessString; out TheOutput: string);
      class procedure ElevateIfNot();
   end;

function AuthorizationStatusToString(AStatus: OSStatus): string;

implementation

uses
   BaseUnix;

(*
typedef struct __sFILE {
    unsigned char *_p;  /* current position in (some) buffer */
    int _r;     /* read space left for getc() */
    int _w;     /* write space left for putc() */
    short   _flags;     /* flags, below; this FILE is free if 0 */
    short   _file;      /* fileno, if Unix descriptor, else -1 */
    struct  __sbuf _bf; /* the buffer (at least 1 byte, if !NULL) */
    int _lbfsize;   /* 0 or -_bf._size, for inline putc */

    /* operations */
    void    *_cookie;   /* cookie passed to io functions */
    int (* _Nullable _close)(void * );
    int (* _Nullable _read) (void *, char *, int);
    fpos_t  (* _Nullable _seek) (void *, fpos_t, int);
    int (* _Nullable _write)(void *, const char *, int);

    /* separate buffer for long sequences of ungetc() */
    struct  __sbuf _ub; /* ungetc buffer */
    struct __sFILEX *_extra; /* additions to FILE to not break ABI */
    int _ur;        /* saved _r when _r is counting ungetc data */

    /* tricks to meet minimum requirements even when malloc() fails */
    unsigned char _ubuf[3]; /* guarantee an ungetc() buffer */
    unsigned char _nbuf[1]; /* guarantee a getc() buffer */

    /* separate buffer for fgetln() when line crosses buffer boundary */
    struct  __sbuf _lb; /* buffer for fgetln() */

    /* Unix stdio files get aligned to block boundaries on fseek() */
    int _blksize;   /* stat.st_blksize (may be != _bf._size) */
    fpos_t  _offset;    /* current lseek offset (see WARNING) */
} FILE;
*)

type
   TCFile = packed record
      _p: Pointer; // sPAnsiChar;
      _r: Integer;
      _w: integer;
      _flags: word;
      _file: word; // gthis is fileno!
      // ...
      _bf: Pointer; // buffer
      _lbfsize: integer;
   end;

function AuthorizationStatusToString(AStatus: OSStatus): string;
begin
   case AStatus of
      errAuthorizationSuccess: begin
         Result := 'errAuthorizationSuccess';
      end;
      errAuthorizationInvalidSet: begin
         Result := 'errAuthorizationInvalidSet';
      end;
      errAuthorizationInvalidRef: begin
         Result := 'errAuthorizationInvalidRef';
      end;
      errAuthorizationInvalidTag: begin
         Result := 'errAuthorizationInvalidTag';
      end;
      errAuthorizationInvalidPointer: begin
         Result := 'errAuthorizationInvalidPointer';
      end;
      errAuthorizationDenied: begin
         Result := 'errAuthorizationDenied';
      end;
      errAuthorizationCanceled: begin
         Result := 'errAuthorizationCanceled';
      end;
      errAuthorizationInteractionNotAllowed: begin
         Result := 'errAuthorizationInteractionNotAllowed';
      end;
      errAuthorizationInternal: begin
         Result := 'errAuthorizationInternal';
      end;
      errAuthorizationExternalizeNotAllowed: begin
         Result := 'errAuthorizationExternalizeNotAllowed';
      end;
      errAuthorizationInternalizeNotAllowed: begin
         Result := 'errAuthorizationInternalizeNotAllowed';
      end;
      errAuthorizationInvalidFlags: begin
         Result := 'errAuthorizationInvalidFlags';
      end;
      errAuthorizationToolExecuteFailure: begin
         Result := 'errAuthorizationToolExecuteFailure';
      end;
      errAuthorizationToolEnvironmentError: begin
         Result := 'errAuthorizationToolEnvironmentError';
      end;
      errAuthorizationBadAddress: begin
         Result := 'errAuthorizationBadAddress';
      end;
      else
      begin
         Result := IntToStr(AStatus);
      end;
   end;
end;
{ TMacOSElevation }

class constructor TMacOSElevation.Create;
begin
   FInstance := nil;
end;

class destructor TMacOSElevation.Destroy;
begin
   FInstance.Free;
end;

class function TMacOSElevation.Instance: TMacOSElevation;
begin
   if not Assigned(FInstance) then begin
      FInstance := TMacOSElevation.Create;
   end;
   Result := FInstance;

end;

constructor TMacOSElevation.Create;
var
   status: OSStatus;
   authFlags: AuthorizationFlags;
   authRights: AuthorizationRights;
   authItem: AuthorizationItem;
begin
   authItem.flags := 0;
   authItem.Name := kAuthorizationRightExecute;
   authItem.Value := nil;
   authItem.valueLength := 0;
   authRights.Count := 1;
   authRights.items := @authItem;
   FAuthenticationReference := nil;
   authFlags := kAuthorizationFlagInteractionAllowed or kAuthorizationFlagExtendRights or kAuthorizationFlagPreAuthorize;
   status := AuthorizationCreate(@authRights, kAuthorizationEmptyEnvironment, authFlags, FAuthenticationReference);
   {$IFDEF DEBUG}
   WriteLn('AuthorizationCreate(...) = ', AuthorizationStatusToString(status));
   {$ENDIF DEBUG}
end;

destructor TMacOSElevation.Destroy;
begin
   AuthorizationFree(FAuthenticationReference, kAuthorizationFlagDestroyRights);
   inherited Destroy;
end;

type
   PCLibFile = uint64;

function fread(__ptr:pointer; __size:size_t; __n:size_t; __stream:PCLibFile):size_t;cdecl;external 'clib' name 'fread';
function fseek(__stream:PCLibFile; __off:longint; __whence:longint):longint;cdecl;external 'clib' name 'fseek';
function fgetc(__stream:PCLibFile):longint;cdecl;external clib name 'fgetc';
function feof(__stream:PCLibFile):longint;cdecl;external clib name 'feof';

procedure TMacOSElevation.RunCommand(AnExecutable: string; const AParameters: array of TProcessString; out TheOutput: string);
var
   status: OSStatus;
   options: AuthorizationFlags;
   a: array of pansichar;
   i: integer;
   pipe: TDirent;
   ppipe: PDirent;
   iBytesRead: integer;
   readBuffer: array[0..127] of ansichar;
   bUseSudo: boolean;
   iSudoInc: integer;
   s: AnsiString;
begin
   TheOutput := '';
   bUseSudo := false;
   options := kAuthorizationFlagDefaults;
   if bUseSudo then begin
      SetLength(a, Length(AParameters) + 2);
      a[0] := pansichar(AnExecutable);
      iSudoInc := 1;
   end else begin
      SetLength(a, Length(AParameters) + 1);
      iSudoInc := 0;
   end;
   for i := Low(AParameters) to High(AParameters) do begin
      s := AParameters[i];
      a[i + iSudoInc] := pansichar(s);
   end;
   a[Pred(Length(a))] := nil;

   // @see https://developer.apple.com/documentation/security/1540038-authorizationexecutewithprivileg
   // @see https://github.com/michaelvobrien/OSXSimpleAuth/blob/master/OSXSimpleAuth.m
   //FillChar(pipe, SizeOf(pipe), 0);
   ppipe := @pipe;
   //ppipe := 0;
   //ppipe := nil;

   (*

   OSStatus AuthorizationExecuteWithPrivileges(
            AuthorizationRef authorization,
            const char *pathToTool,
            AuthorizationFlags options,
            char *const
            _Nonnull *arguments,
            FILE * _Nullable *communicationsPipe);

   function AuthorizationExecuteWithPrivileges(
            authorization: AuthorizationRef;
            pathToTool: CStringPtr;
            options: AuthorizationFlags;
            arguments: Arg10000TypePtr;
            communicationsPipe: UnivPtr): OSStatus; external name '_AuthorizationExecuteWithPrivileges';

   *)
   if bUseSudo then begin
      status := AuthorizationExecuteWithPrivileges(FAuthenticationReference, pansichar('/usr/bin/sudo'), options, Arg10000TypePtr(a), ppipe);
   end else begin
      status := AuthorizationExecuteWithPrivileges(FAuthenticationReference, pansichar(AnExecutable), options, Arg10000TypePtr(a), ppipe);
   end;
   {$IFDEF DEBUG}
   WriteLn('AuthorizationExecuteWithPrivileges(...) = ', AuthorizationStatusToString(status));
   {$ENDIF DEBUG}
   (*
    char readBuffer[128];
    if (status == errAuthorizationSuccess) {
        for (;;) {
            int bytesRead = read(fileno(pipe), readBuffer, sizeof(readBuffer));
            if (bytesRead < 1) break;
            write(fileno(stdout), readBuffer, bytesRead);
        }
    } else {
        NSLog(@"Authorization Result Code: %d", status);
    }
   *)
   TheOutput += '>>>';
   if (errAuthorizationSuccess = status) then begin
      Sleep(500);
      (*
      while not feof(ppipe)=0 do begin
         i := fgetc(ppipe);
         TheOutput += Char(i);
      end;
      *)

      (*
      repeat
         // we need to use fileno(pipe) here!
         //iBytesRead := FileRead(ppipe^.Entry2, readBuffer, SizeOf(readBuffer));
         iBytesRead := 0;
         // iBytesRead := fread(@readBuffer[0], SizeOf(readBuffer), 1, LongInt(ppipe));
         TheOutput += '(' + IntToStr(iBytesRead) + ')';
         for i := 0 to Pred(iBytesRead) do begin
            TheOutput += readBuffer[i];
         end;
         if (-1 = iBytesRead) then begin
            TheOutput += SysErrorMessage(fpgeterrno);
         end;
      until (iBytesRead <= 0);
      // FileClose(ppipe);
      *)
   end;
   TheOutput += '<<<';
end;

class procedure TMacOSElevation.ElevateIfNot();
var
   s: string;
begin
   {$IFDEF DEBUG}
   WriteLn('UID: ' + IntToStr(FpGetUID()));
   {$ENDIF DEBUG}
   if (0 <> FpGetUID()) and (not FindCmdLineSwitch('force-elevation')) then begin
      {$IFDEF DEBUG}
      WriteLn('About to elevate: ' + Application.ExeName);
      {$ENDIF DEBUG}
      TMacOSElevation.Instance.RunCommand(Application.ExeName, ['-force-elevation'], s);
      Halt(0);
   end;
end;

end.
