{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provided Pascal class to add MacOS toolbar.)

   @see https://www.lazarusforum.de/viewtopic.php?f=23&t=15347
   @see https://developer.apple.com/documentation/appkit/touch_bar/integrating_a_toolbar_and_touch_bar_into_your_app?language=objc

   Thanks to user Rhyt on the German Lazarus forums for coming up with the idea,
   and providing examples. Without these, I wouldn't have tried or would have
   spent way more time on this.

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// Uses code from Rhyt @ https://www.lazarusforum.de/viewtopic.php?f=23&t=15347
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Usage example:
// ---------------------------------------
// Set the custom option -WM11.0
//
// procedure TFormNetworkLeprechaun.AddToolbar;
// var
//    toolbar: TMacToolbar;
//    item: TMacToolbarItem;
// begin
//   toolbar := TMacToolbar.Create(Self);
//   item := toolbar.Items.AddItem('refreshToolbarItem', 'Refresh', 'Refresh', Self.FActionRefresh);
//   item.ImageFilename := 'Resources/rotate.png';
//   item := toolbar.Items.AddItem('connectToolbarItem', 'Connect', 'Connect', Self.FActionConnect);
//   item.ImageFilename := 'Resources/link.png';
//   item := toolbar.Items.AddItem('disconnectToolbarItem', 'Disconnect', 'Disconnect', Self.FActionDisconnect);
//   item.ImageFilename := 'Resources/link-slash.png';
//   toolbar.Attach;
// end;
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-02-02  pk   5m  Added the NSHelpers unit instead of copying code from it.
// 2024-02-02  pk  30m  Working code moved from app into separate unit.
// 2024-02-01  pk  30m  Draft.
// *****************************************************************************
// TODO
// - [ ] Finish implemenation of IsCentered
// - [ ] Standard component
// - [ ] Delete when active (need subitem support?)
// - [ ] Add when active
// - [ ] ImageList bitmap transparency
// - [ ] Visible state
// - [ ] For a system image picker, check NSImage to bitmap, https://forum.lazarus.freepascal.org/index.php?topic=32356.0
// *****************************************************************************
   )
}
unit Firefly.MacOS.Toolbar;

{$mode Delphi}{$H+}
{$modeswitch objectivec1}

{$IFDEF LCLCarbon}
{$WARNING Please compile using the Cocoa widgetset.}
// Project -> Project Options... -> Compiler Options -> Config and Target -> Select another target widgetset -> Cocoa
// or
// Project -> Project Options... -> Compiler Options -> Additions and Overrides -> Add -> IDE Macro: LCLWidgetType:=cocoa
{$ENDIF LCLCarbon}

{$IFDEF LCLCocoa}
{$IF MAC_OS_X_VERSION_MIN_REQUIRED <= 1099}
{$WARNING Add Custom Option -WM11.0 to support search fields!}
// Project -> Project Options... -> Compiler Options -> Additions and Overrides -> Add -> Custom Option -> "-WM11.0".
{$ENDIF}
{$ENDIF LCLCocoa}

interface

uses
   Classes,
   SysUtils,
   Forms,
   ActnList,
   Dialogs,
   Menus,
   Graphics,
   ImgList,
   Controls,
   CocoaAll,
   Firefly.MacOS.Toolbar.Items,
   Firefly.MacOS.Toolbar.Headers,
   Firefly.MacOS.ClassHelpers;

type
   TMacToolbarDisplayMode = (mtdmDefault, mtdmIconAndLabel, mtdmIconOnly, mdtmLabelOnly);
   TMacToolbarSizeMode = (mtsmDefault, mtsmRegular, mtsmSmall);

   TMacToolbarDelegate = objcclass;

   { TMacToolbar }

   TMacToolbar = class(TComponent)
   private
      FAllowsUserCustomization: boolean;
      FAutosavesConfiguration: boolean;
      FImageList: TCustomImageList;
      FShowsBaseLineSeparator: boolean;
      FDisplayMode: TMacToolbarDisplayMode;
      FForm: TCustomForm;
      FItems: TMacToolbarItemList;
      FMacWindow: NSWindow;
      FDelegate: TMacToolbarDelegate;
      FSizeMode: TMacToolbarSizeMode;
      FToolbarIdentifier: string;
      function GetSelectedItem: TMacToolbarItem;
      procedure SetImageList(AValue: TCustomImageList);
      procedure SetSelectedItem(AValue: TMacToolbarItem);
   protected
      FMacToolbar: NSToolbar;
   public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      function Attach: boolean; // call once all entries have been added!
      property SelectedItem: TMacToolbarItem read GetSelectedItem write SetSelectedItem;
   published
      property Items: TMacToolbarItemList read FItems;
      property AllowsUserCustomization: boolean read FAllowsUserCustomization write FAllowsUserCustomization default true;
      property AutosavesConfiguration: boolean read FAutosavesConfiguration write FAutosavesConfiguration default true;
      property DisplayMode: TMacToolbarDisplayMode read FDisplayMode write FDisplayMode default mtdmDefault;
      property SizeMode: TMacToolbarSizeMode read FSizeMode write FSizeMode default mtsmDefault;
      property ShowsBaseLineSeparator: boolean read FShowsBaseLineSeparator write FShowsBaseLineSeparator default false;
      property ToolbarIdentifier: string read FToolbarIdentifier write FToolbarIdentifier;
      property ImageList: TCustomImageList read FImageList write SetImageList;
   end;

   { TMacToolbarDelegate }

   TMacToolbarDelegate = objcclass(NSObject,
         {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100} NSSearchFieldDelegateProtocol, {$ENDIF}
         NSSharingServicePickerToolbarItemDelegateProtocol,
         NSToolbarDelegateProtocol)
      function toolbar_itemForItemIdentifier_willBeInsertedIntoToolbar({%H-}toolbar: NSToolbar; itemIdentifier: NSString; {%H-}flag: boolean): NSToolbarItem; message 'toolbar:itemForItemIdentifier:willBeInsertedIntoToolbar:';
      function toolbarAllowedItemIdentifiers({%H-}toolbar: NSToolbar): NSArray; message 'toolbarAllowedItemIdentifiers:';
      function toolbarDefaultItemIdentifiers({%H-}toolbar: NSToolbar): NSArray; message 'toolbarDefaultItemIdentifiers:';
      function toolbarImmovableItemIdentifiers({%H-}toolbar: NSToolbar): NSArray; message 'toolbarImmovableItemIdentifiers:';
      function toolbarSelectableItemIdentifiers({%H-}toolbar: NSToolbar): NSArray; message 'toolbarSelectableItemIdentifiers:';
      // NSSharingServicePickerToolbarItemDelegateProtocol
      function itemsForSharingServicePickerToolbarItem(pickerToolbarItem: NSSharingServicePickerToolbarItem): NSArray; message 'itemsForSharingServicePickerToolbarItem:';
      // NSSearchFieldDelegateProtocol
      procedure searchFieldDidStartSearching(sender: NSSearchField); message 'searchFieldDidStartSearching:';
      procedure searchFieldDidEndSearching(sender: NSSearchField); message 'searchFieldDidEndSearching:';
      // NSControlTextEditingDelegateProtocol
      procedure controlTextDidChange(obj: NSNotification); message 'controlTextDidChange:'; override;
   private
      FItems: TMacToolbarItemList;
      procedure toolbarItemClick(item: NSToolbarItem); message 'toolbarItemClick:';
      procedure toolbarItemGroupClick({%H-}item: NSToolbarItemGroup); message 'toolbarItemGroupClick:';
      procedure segmentedControlClick(control: NSSegmentedControl); message 'segmentedControlClick:';
   public
      function validateToolbarItem(theItem: NSToolbarItem): ObjCBOOL; message 'validateToolbarItem:'; override;
   end;

implementation

{ TMacToolbar }

function TMacToolbar.GetSelectedItem: TMacToolbarItem;
begin
   Result := FItems.FindItemWithIdentifier(FMacToolbar.selectedItemIdentifier, true);
end;

procedure TMacToolbar.SetImageList(AValue: TCustomImageList);
begin
   FImageList := AValue;
   FItems.ImageList := AValue;
end;

procedure TMacToolbar.SetSelectedItem(AValue: TMacToolbarItem);
begin
   FMacToolbar.setSelectedItemIdentifier(NSSTR(AValue.Identifier));
end;

constructor TMacToolbar.Create(AOwner: TComponent);
begin
   inherited;
   if AOwner is TCustomForm then begin
      FForm := TCustomForm(AOwner);
   end else begin
      FForm := nil;
   end;
   FItems := TMacToolbarItemList.Create;
   FDisplayMode := mtdmDefault;
   FSizeMode := mtsmDefault;
   FAllowsUserCustomization := true;
   FShowsBaseLineSeparator := false; // default
   FToolbarIdentifier := AOwner.ClassName + 'LazarusMacToolbar' + FormatDateTime('yyyymmddhhnnsszzz', Now);
   FMacToolbar := NSToolbar.alloc;
   FDelegate := TMacToolbarDelegate.alloc.init;
end;

destructor TMacToolbar.Destroy;
begin
   FMacToolbar.release;
   FDelegate.release;
   FItems.Free;
   inherited Destroy;
end;

function TMacToolbar.Attach: boolean;
begin
   Result := false;
   if not Assigned(FForm) then begin
      Exit;
   end;
   try
      FMacWindow := {%H-}NSView(FForm.Handle).window;
      {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
      FMacToolbar.initWithIdentifier(NSSTR(FToolbarIdentifier));
      {$ELSE}
      FMacToolbar.init;
      {$ENDIF}
      FDelegate.FItems := Self.Items;
      FMacToolbar.setDelegate(FDelegate);
      FMacToolbar.setAllowsUserCustomization(FAllowsUserCustomization);
      FMacToolbar.setAutosavesConfiguration(FAutosavesConfiguration);
      case FDisplayMode of
         mtdmDefault: begin
            FMacToolbar.setDisplayMode(NSToolbarDisplayModeDefault);
         end;
         mtdmIconAndLabel: begin
            FMacToolbar.setDisplayMode(NSToolbarDisplayModeIconAndLabel);
         end;
         mtdmIconOnly: begin
            FMacToolbar.setDisplayMode(NSToolbarDisplayModeIconOnly);
         end;
         mdtmLabelOnly: begin
            FMacToolbar.setDisplayMode(NSToolbarDisplayModeLabelOnly);
         end;
      end;
      case FSizeMode of
         mtsmDefault: begin
            FMacToolbar.setSizeMode(NSToolbarSizeModeDefault);
         end;
         mtsmRegular: begin
            FMacToolbar.setSizeMode(NSToolbarSizeModeRegular);
         end;
         mtsmSmall: begin
            FMacToolbar.setSizeMode(NSToolbarSizeModeSmall);
         end;
      end;
      FMacToolbar.setShowsBaselineSeparator(FShowsBaseLineSeparator);
      FMacWindow.setToolbar(FMacToolbar);
      FItems.MacToolbar := FMacToolbar;
      Result := true;
   except
      Result := false;
   end;
end;

{ TMacToolbarDelegate }

{
  Asks the delegate for the toolbar item associated with the specified identifier.

  Use this method to create new NSToolbarItem objects when the toolbar asks for
  them. If your toolbar item uses a custom view, make sure that view is fully
  configured before you return the item. The toolbar becomes the owner of the
  returned item, but can display the item either in the toolbar or the
  customization palette.

  Don’t recycle toolbar items; always provide a new instance, even if the
  toolbar previously asked for an item with the same identifier.

  Even though this is an optional method, you must implement it if you create
  the toolbar programatically.

  @see https://developer.apple.com/documentation/appkit/nstoolbardelegate/1516985-toolbar?language=objc

  @param toolbar NSToolbar        The toolbar for which the item is being requested.
  @param idemIdentifier NSString  The identifier for the requested item.
  @param flag boolean             YES if the toolbar will insert the item immediately.
                                  If this parameter is NO, provide a canonical
                                  representation for the item. For example, provide
                                  a version of the item suitable for display in
                                  the toolbar customization sheet.

  @return NSToolbarItem  A new NSToolbarItem object, or nil if no toolbar item is available
                         for the specified identifier.
}
function TMacToolbarDelegate.toolbar_itemForItemIdentifier_willBeInsertedIntoToolbar(toolbar: NSToolbar; itemIdentifier: NSString; flag: boolean): NSToolbarItem;
var
   item: TMacToolbarItem;
begin
   Result := nil;
   item := FItems.FindItemWithIdentifier(itemIdentifier);
   if Assigned(item) then begin
      case item.ItemType of
         mtitItem: begin
            Result := item.GetItem(Self);
         end;
         mtitSearchItem: begin
            if item is TMacToolbarSearchItem then begin
               {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
               Result := TMacToolbarSearchItem(item).GetSearchItem(Self);
               {$ENDIF}
            end;
         end;
         mtitMenuItem: begin
            if item is TMacToolbarMenuItem then begin
               Result := TMacToolbarMenuItem(item).GetMenuItem(Self);
            end;
         end;
         mtitItemGroup: begin
            if item is TMacToolbarItemgroup then begin
               Result := TMacToolbarItemgroup(item).GetItemGroup(Self);
            end;
         end;
         mtitShareItem: begin
            if item is TMacToolbarShareItem then begin
               Result := TMacToolbarShareItem(item).GetShareItem(Self);
            end;
         end;
      end;
   end;
      // 1. custom buttons would use toolbarItem.setView
      // can we use this for e.g. TListbox or something similar?
      // 2. possible support of TPopupMenu: use this:
      (*
      let menuItem: NSMenuItem = NSMenuItem()
      menuItem.submenu = nil
      menuItem.title = label
      toolbarItem.menuFormRepresentation = menuItem
      *)
end;


{
  Asks the delegate to provide the items allowed on the toolbar.

  Include all of your toolbar’s items, including standard ones defined by
  NSToolbarIdentifier. The array must include all of the default menu items in
  your toolbar.

  Even though this is an optional method, you must implement it if you create
  the toolbar programatically.

  @see https://developer.apple.com/documentation/appkit/nstoolbardelegate/1516995-toolbaralloweditemidentifiers?language=objc

  @param toolbar NSToolbar  The toolbar whose allowed item identifiers are to be returned.

  @return NSArray<NSToolbarItemIdentifier>  An array of toolbar item identifiers,
          each of which represents an item that appears in the customization palette.
          Arrange the identifiers in the order you want them to appear in the
          palette, with the first item appearing on the palette’s leading edge.
}
function TMacToolbarDelegate.toolbarAllowedItemIdentifiers(toolbar: NSToolbar): NSArray;
begin
   Result := FItems.GetAllowedItemsArray;
end;

{
  Asks the delegate to provide the default items to display on the toolbar.

  The toolbar calls this method when user settings don’t contain any custom
  configuration data for the toolbar. The toolbar also calls it to initialize
  the customization palette’s contents.

  Even though this is an optional method, you must implement it if you create
  the toolbar programatically. If you configure your toolbar in Interface
  Builder, the configuration there provides the default items.

  @see https://developer.apple.com/documentation/appkit/nstoolbardelegate/1516944-toolbardefaultitemidentifiers?language=objc

  @param toolbar NSToolbar  The toolbar whose default item identifiers are to be returned.

  @return NSArray<NSToolbarItemIdentifier>  An array of toolbar item identifiers,
          each of which represents an item that appears in the default toolbar.
          Arrange the identifiers in the order you want them to appear in the
          toolbar, with the first item appearing on the toolbar’s leading edge.
}
function TMacToolbarDelegate.toolbarDefaultItemIdentifiers(toolbar: NSToolbar): NSArray;
begin
   Result := FItems.GetDefaultItemsArray;
end;

{
  Asks the delegate to provide the items that people can’t remove from the
  toolbar or rearrange during the customization process.

  Implement this method in your delegate and return any items you don’t want
  people to remove or rearrange. If you don’t implement this method, the toolbar
  lets people rearrange and remove all toolbar items.

  @see https://developer.apple.com/documentation/appkit/nstoolbardelegate/3969240-toolbarimmovableitemidentifiers?language=objc

  @param toolbar NSToolbar  The toolbar that contains the items.

  @return NSArray<NSToolbarItemIdentifier>  The set of item identifiers that people
          can’t remove from the toolbar or move to other locations in the toolbar.
          Return an empty set to let someone customize all toolbar items.
}

function TMacToolbarDelegate.toolbarImmovableItemIdentifiers(toolbar: NSToolbar): NSArray;
begin
   Result := FItems.GetImmovableItemsArray;
end;


{
  Asks the delegate to provide the set of selectable items in the toolbar.

  An array of item identifiers, each of which corresponds to an NSToolbarItem
  that should display a selection indicator in the specified toolbar.

  Use this method to return the complete list of toolbar items that support
  selection. When someone selects one of the returned items, the toolbar
  automatically displays that item with a visual highlight. The toolbar also
  places the currently selected item in its selectedItemIdentifier property.

  @see https://developer.apple.com/documentation/appkit/nstoolbardelegate/1516981-toolbarselectableitemidentifiers?language=objc

  @param toolbar NSToolbar  The toolbar that contains the items.

  @return NSArray<NSToolbarItemIdentifier>  An array of item identifiers, each
          of which corresponds to an NSToolbarItem that should display a
          selection indicator in the specified toolbar.
}

function TMacToolbarDelegate.toolbarSelectableItemIdentifiers(toolbar: NSToolbar): NSArray;
begin
   Result := FItems.GetSelectableItemsArray;
end;

function TMacToolbarDelegate.itemsForSharingServicePickerToolbarItem(pickerToolbarItem: NSSharingServicePickerToolbarItem): NSArray;
var
   sl: TStringList;
   mtitem: TMacToolbarItem;
begin
   if not Assigned(pickerToolbarItem) then begin
      Exit;
   end;
   mtitem := FItems.FindItemWithIdentifier(pickerToolbarItem.itemIdentifier);
   if Assigned(mtitem) and (mtitem is TMacToolbarShareItem) and Assigned(TMacToolbarShareItem(mtitem).OnGetSharedStrings) then begin
      sl := TStringList.Create;
      try
         TMacToolbarShareItem(mtitem).OnGetSharedStrings(sl);
         Result := sl.ToNSArray;
      finally
         sl.Free;
      end;
   end;
end;

procedure TMacToolbarDelegate.searchFieldDidStartSearching(sender: NSSearchField);
var
   mtitem: TMacToolbarSearchItem;
begin
   if not Assigned(sender) then begin
      Exit;
   end;
   mtitem := FItems.FindItemWithSearchField(sender, false);
   if Assigned(mtitem) and Assigned(mtitem.OnBeginSearch) then begin
      mtitem.OnBeginSearch(mtitem);
   end;
end;

procedure TMacToolbarDelegate.searchFieldDidEndSearching(sender: NSSearchField);
var
   mtitem: TMacToolbarSearchItem;
begin
   if not Assigned(sender) then begin
      Exit;
   end;
   mtitem := FItems.FindItemWithSearchField(sender, false);
   if Assigned(mtitem) and Assigned(mtitem.OnEndSearch) then begin
      mtitem.OnEndSearch(mtitem);
   end;
   sender.resignFirstResponder;
end;

procedure TMacToolbarDelegate.controlTextDidChange(obj: NSNotification);
var
   mtitem: TMacToolbarItem;
begin
   if not Assigned(obj) then begin
      Exit;
   end;
   mtitem := FItems.FindItemWithObject(obj.object_, true);
   if Assigned(mtitem) and (mtitem is TMacToolbarSearchItem) and Assigned(TMacToolbarSearchItem(mtitem).OnChange) then begin
      TMacToolbarSearchItem(mtitem).OnChange(mtitem);
   end;
end;

{
  If this method is implemented and returns NO, NSToolbar will disable theItem; re
  turning YES causes theItem to be enabled.

  If the receiver is the target for the actions of multiple toolbar items,
  it’s necessary to determine which toolbar item theItem refers to by testing
  the itemIdentifier.

  @see https://developer.apple.com/documentation/objectivec/nsobject/1524282-validatetoolbaritem
}
function TMacToolbarDelegate.validateToolbarItem(theItem: NSToolbarItem): ObjCBOOL;
var
   mtitem: TMacToolbarItem;
begin
   Result := false;
   if not Assigned(theItem) then begin
      Exit;
   end;
   mtitem := FItems.FindItemWithIdentifier(theItem.itemIdentifier);
   if Assigned(mtitem) then begin
      Result := mtitem.Enabled;
   end;
end;

procedure TMacToolbarDelegate.toolbarItemClick(item: NSToolbarItem);
var
   mtitem: TMacToolbarItem;
begin
   if not Assigned(item) then begin
      Exit;
   end;
   mtitem := FItems.FindItemWithIdentifier(item.itemIdentifier);
   if Assigned(mtitem) then begin
      if Assigned(mtitem.Action) then begin
         mtitem.Action.Execute;
      end;
   end;
end;

procedure TMacToolbarDelegate.toolbarItemGroupClick(item: NSToolbarItemGroup);
begin
   // seems to not be triggered, use segmentedControlClick instead.
end;

procedure TMacToolbarDelegate.segmentedControlClick(control: NSSegmentedControl);
var
   item: TMacToolbarItem;
   subItem: TMacToolbarItem;
   i: integer;
begin
   for item in FItems do begin
      if (item is TMacToolbarItemGroup) then begin
         if (TMacToolbarItemGroup(item).SegmentedControl = control) then begin
            i := control.selectedSegment;
            if (i < item.SubItems.Count) then begin
               subItem := item.SubItems[i];
               if Assigned(subItem.Action) then begin
                  subItem.Action.Execute;
               end;
            end;
         end;
      end;
   end;
end;

end.
