{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(macOS dock items.)

   @see https://wiki.lazarus.freepascal.org/macOS_Application_Dock_Menu

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
   )
}
unit Firefly.MacOS.App;

{$mode objfpc}{$H+}
{$modeswitch objectivec1}

interface

uses
   Classes,
   SysUtils,
   Menus,
   Forms,
   LCLType,
   CocoaAll;

type
  { TMacOSAppDelegate }

  TMacOSAppDelegate = objcclass(NSObject, NSApplicationDelegateProtocol)
  protected
     FMenuItems: TPopupmenu;
  public
     function applicationDockMenu({%H-}sender: NSApplication): NSMenu; message 'applicationDockMenu:';
     procedure menuItemClick(menuItem: NSMenuItem); message 'menuItemClick:';
  end;

  { TMacOSAppHelper }

  TMacOSAppHelper = class(TComponent)
  private
     FDelegate: TMacOSAppDelegate;
     FMenuItems: TPopupmenu;
  protected
  public
     constructor Create({%H-}AOwner: TComponent); override;
     destructor Destroy; override;
     procedure Attach;
  published
     property MenuItems: TPopupmenu read FMenuItems write FMenuItems;
  end;

implementation

{ TMacOSAppDelegate }

function TMacOSAppDelegate.applicationDockMenu(sender: NSApplication): NSMenu;
var
  //aSel: SEL;
  myMenu: NSMenu;
  //mySubMenu: NSMenu;
  myMenuItem: NSMenuItem;
  //mi: TMenuItem;
begin
   if Assigned(FMenuItems) then begin
      Result := NSMenu(FMenuItems.Handle);
   end else begin
      myMenu := NSMenu.alloc.init.autorelease;
      myMenuItem := NSMenuItem.alloc.initWithTitle_action_keyEquivalent(NSStr(Application.Title), Nil, NSStr('')).autorelease;
      myMenu.addItem(myMenuItem);
      // FMenuItems is empty, so the below cannot work
      // it would a solution to add items is we couldn't convert the handle to NSMenu
      (*
      aSel := ObjCSelector(TMacOSAppDelegate.menuItemClick);
      for mi in FMenuItems.Items do begin
         myMenuItem := NSMenuItem.alloc.initWithTitle_action_keyEquivalent(NSStr(mi.Caption), aSel, NSStr('')).autorelease;
         myMenuItem.setTag(NSInteger(mi));
         myMenu.addItem(myMenuItem);
         if (mi.Count > 0) then begin
            mySubMenu := NSMenu.alloc.init.autorelease;
            myMenuItem.setSubmenu(mySubMenu);
         end;
      end;
      *)
      Result := myMenu;
   end;
end;

procedure TMacOSAppDelegate.menuItemClick(menuItem: NSMenuItem);
var
   mi: TMenuItem;
begin
   mi := TMenuItem(menuItem.tag);
   if Assigned(mi.Action) then begin
      mi.Action.Execute;
   end else if Assigned(mi.OnClick) then begin
      mi.OnClick(nil);
   end;
end;

{ TMacOSAppHelper }

constructor TMacOSAppHelper.Create(AOwner: TComponent);
begin
   inherited;
   FDelegate := TMacOSAppDelegate.alloc.init;
end;

destructor TMacOSAppHelper.Destroy;
begin
   FDelegate.Release;
   inherited;
end;

procedure TMacOSAppHelper.Attach;
begin
   FDelegate.FMenuItems := FMenuItems;
   NSApplication.sharedApplication.setDelegate(FDelegate);
end;

end.

