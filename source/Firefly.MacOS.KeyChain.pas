{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Access to the macOS keychain.)

   @see https://stackoverflow.com/questions/23828515/storing-data-in-ios-device-keychain-via-delphi-xe
   @see https://developer.apple.com/documentation/security/1398306-secitemcopymatching
   @see https://stackoverflow.com/questions/9864234/cocoa-interface-to-macos-x-keychain
   @see https://github.com/samsoffes/sskeychain/blob/master/SSKeychain.m

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-24  pk   5m  Added header.
// 2024-01-24  pk  15m  Abstracted code from Wireguard.KeyChain unit.
// *****************************************************************************
   )
}
unit Firefly.MacOS.KeyChain;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   MacOSAll;

type

   { TKeyChainEntry }

   TKeyChainEntry = class
   private
      FData: string;
      FDescription: string;
      FLabel: string;
      FService: string;
   public
      procedure FromItem(AnItem: KCItemRef; AnIncludeData: boolean = False);
   published
      property Description: string read FDescription;
      property ItemLabel: string read FLabel;
      property Service: string read FService;
      property Data: string read FData;
   end;

   { TKeyChainSearch }

   TKeyChainSearch = class(TObjectList<TKeyChainEntry>)
   protected
      function Enum(TheAttributes: KCAttributeList; AnIncludeData: boolean = False): boolean;
   public
      function ListGenericPasswords(AnIncludeData: boolean = False): boolean;
      function FindGenericPasswordsForService(AServiceName: string; AnIncludeData: boolean = False): boolean;
   end;

(*
   // Example for porting commands

   OSStatus AuthorizationExecuteWithPrivileges(
            AuthorizationRef authorization,
            const char *pathToTool,
            AuthorizationFlags options,
            char *const
            _Nonnull *arguments,
            FILE * _Nullable *communicationsPipe);

   function AuthorizationExecuteWithPrivileges(
            authorization: AuthorizationRef;
            pathToTool: CStringPtr;
            options: AuthorizationFlags;
            arguments: Arg10000TypePtr;
            communicationsPipe: UnivPtr): OSStatus; external name '_AuthorizationExecuteWithPrivileges';

   *)


(*
  func SecItemCopyMatching(
    _ query: CFDictionary,
    _ result: UnsafeMutablePointer<CFTypeRef?>?
    ) -> OSStatus
*)

   // function SecItemCopyMatching(query; CFDictionary): OSStatus; external Name '_SecItemCopyMatching';

implementation

{ TKeyChainEntry }

procedure TKeyChainEntry.FromItem(AnItem: KCItemRef; AnIncludeData: boolean);
var
   status: OSStatus;
   iMaxLen: uint32;
   iActualLen: uint32;
   pc: pansichar;
   attr1: KCAttribute;
begin
   iMaxLen := 1024;
   iActualLen := 0;
   pc := AllocMem(iMaxLen);
   try
      FillChar(pc^, iMaxLen, 0);
      // e.g. wg-quick(8) config
      attr1.tag := kDescriptionKCItemAttr;
      attr1.length := iMaxLen;
      attr1.Data := pc;
      status := KCGetAttribute(AnItem, attr1, iActualLen);
      if (status = 0) then begin
         Self.FDescription := ansistring(pc);
      end;

      FillChar(pc^, iMaxLen, 0);
      attr1.tag := kLabelKCItemAttr;
      attr1.length := iMaxLen;
      attr1.Data := pc;
      status := KCGetAttribute(AnItem, attr1, iActualLen);
      if (status = 0) then begin
         Self.FLabel := ansistring(pc);
      end;

      FillChar(pc^, iMaxLen, 0);
      attr1.tag := kServiceKCItemAttr;
      attr1.length := iMaxLen;
      attr1.Data := pc;
      status := KCGetAttribute(AnItem, attr1, iActualLen);
      if (status = 0) then begin
         Self.FService := ansistring(pc);
      end;

      if AnIncludeData then begin
         FillChar(pc^, iMaxLen, 0);
         status := KCGetData(AnItem, iMaxLen, pc, iActualLen);
         if (0 = status) then begin
            Self.FData := ansistring(pc);
         end;
      end;
   finally
      FreeMem(pc);
   end;
end;

{ TKeyChainSearch }

function TKeyChainSearch.Enum(TheAttributes: KCAttributeList; AnIncludeData: boolean): boolean;
var
   status: OSStatus;
   keychain: KCRef;
   search: KCSearchRef;
   item: KCItemRef;
   entry: TKeyChainEntry;
begin
   Result := True;
   Initialize(keychain);
   status := KCGetDefaultKeychain(keychain);
   Result := (0 = status);
   if not Result then begin
      Exit;
   end;

   search := nil;
   item := nil;
   status := KCFindFirstItem(keychain, @TheAttributes, search, item);
   if (0 = status) then begin
      try
         repeat
            entry := TKeyChainEntry.Create;
            entry.FromItem(item, AnIncludeData);
            Self.Add(entry);
            KCReleaseItem(item);
            status := KCFindNextItem(search, item);
         until (status <> 0);
      finally
         KCReleaseSearch(search);
      end;
   end;
end;

function TKeyChainSearch.FindGenericPasswordsForService(AServiceName: string; AnIncludeData: boolean): boolean;
var
   pcServiceName: pansichar;
   attrList: KCAttributeList;
   attra: array[0..1] of KCAttribute;
   itemClass: KCItemClass;
begin
   itemClass := kGenericPasswordKCItemClass;
   attra[0].tag := kClassKCItemAttr;
   attra[0].Data := @itemClass;
   attra[0].length := sizeof(itemClass);

   pcServiceName := pansichar(AServiceName);
   attra[1].tag := kServiceKCItemAttr;
   attra[1].Data := pcServiceName;
   attra[1].length := Length(pcServiceName);

   attrList.Count := 2;
   attrList.attr := @attra[0];

   Result := Enum(attrList, AnIncludeData);
end;

function TKeyChainSearch.ListGenericPasswords(AnIncludeData: boolean): boolean;
var
   attrList: KCAttributeList;
   attra: array[0..0] of KCAttribute;
   itemClass: KCItemClass;
begin
   itemClass := kGenericPasswordKCItemClass;
   attra[0].tag := kClassKCItemAttr;
   attra[0].Data := @itemClass;
   attra[0].length := sizeof(itemClass);

   attrList.Count := 1;
   attrList.attr := @attra[0];

   Result := Enum(attrList, AnIncludeData);
end;

end.
