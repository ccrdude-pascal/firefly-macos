{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(macOS toolbar related header translations.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
   )
}
unit Firefly.MacOS.Toolbar.Headers;

{$mode Delphi}{$H+}
{$modeswitch objectivec1}

interface

uses
   Classes,
   {$IFDEF LCLCocoa}
   CocoaAll,
   {$ENDIF LCLCocoa}
   SysUtils;

{$IFDEF LCLCocoa}

type
   {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
   NSSearchFieldEx = objcclass external (NSSearchField)
   public
      // @property (nullable, weak) id<NSSearchFieldDelegate> delegate API_AVAILABLE(macos(10.11));
      procedure setDelegate(newValue: id); message 'setDelegate:';
      function delegate: id; message 'delegate';
   end;

   NSControlTextEditingDelegateProtocolEx = objcprotocol external  (NSControlTextEditingDelegateProtocol)
   optional
      //- (void)controlTextDidBeginEditing:(NSNotification *)obj NS_SWIFT_UI_ACTOR;
      //- (void)controlTextDidEndEditing:(NSNotification *)obj NS_SWIFT_UI_ACTOR;
      //- (void)controlTextDidChange:(NSNotification *)obj NS_SWIFT_UI_ACTOR;
      procedure controlTextDidBeginEditing(obj: NSNotification); message 'controlTextDidChange:';
      procedure controlTextDidEndEditing(obj: NSNotification); message 'controlTextDidChange:';
      procedure controlTextDidChange(obj: NSNotification); message 'controlTextDidChange:';
   end;

   {
     NSSearchToolbarItem provides the standard UI behavior for integrating a
     search field into the toolbar.

     @see https://developer.apple.com/documentation/appkit/nssearchtoolbaritem?language=objc
     @see NSSearchToolbarItem.h
   }
   NSSearchToolbarItem = objcclass external (NSToolbarItem)
   public
      procedure setSearchField(newValue: NSSearchField); message 'setSearchField:';
      {
        An NSSearchField displayed in the toolbar item. While inside the toolbar
        item, the field properties and layout constraints are managed by the
        item. The field should be configured before assigned. The width
        constraint for the field could be updated after assigned. When set to
        nil, will reset to a search field with the default configuration.

        // @property (strong) NSSearchField *searchField API_UNAVAILABLE(macCatalyst);
      }
      function searchField: NSSearchFieldEx; message 'searchField';

      {
        When YES, the cancel button in the field resigns the first responder
        status of the search field as clearing the contents. The default is YES.

        // @property BOOL resignsFirstResponderWithCancel;
      }
      function resignsFirstResponderWithCancel: ObjCBool; message 'resignsFirstResponderWithCancel';
      {
        The preferred width for the search field. This value is used to
        configure the search field width whenever it gets the keyboard focus.
        If specifying custom width constraints to the search field, they should
        not conflict with this value.

        // @property CGFloat preferredWidthForSearchField;
      }
      // function preferredWidthForSearchField: CGFloat; message 'preferredWidthForSearchField';
      {
        Starts a search interaction. If necessary, expands to the preferred
        width and moves the keyboard focus to the search field.

        // - (void)beginSearchInteraction;
      }
      procedure beginSearchInteraction; message 'beginSearchInteraction';
      {
        Ends a search interaction. Gives up the first responder by calling
        -endEditing: to the search field. Adjusts to the natural available
        width for the toolbar item if necessary.

        // - (void)endSearchInteraction;
      }
      procedure endSearchInteraction; message 'endSearchInteraction';
   end;

   NSSearchFieldDelegateProtocol = objcprotocol external name 'NSSearchFieldDelegate' (NSTextFieldDelegateProtocol)
   optional
      // - (void)searchFieldDidStartSearching:(NSSearchField *)sender NS_SWIFT_UI_ACTOR API_AVAILABLE(macos(10.11));
      // - (void)searchFieldDidEndSearching:(NSSearchField *)sender NS_SWIFT_UI_ACTOR API_AVAILABLE(macos(10.11));
      procedure searchFieldDidStartSearching(sender: NSSearchField); message 'searchFieldDidStartSearching:';
      procedure searchFieldDidEndSearching(sender: NSSearchField); message 'searchFieldDidEndSearching:';
   end;
   {$ENDIF}



   {

     @see https://developer.apple.com/documentation/appkit/nssharingservicepicker
     @see NSSharingServicePickerToolbarItem.h
   }
   {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1015}
   NSSharingServicePickerToolbarItem = objcclass external (NSToolbarItem)
   public
      // @property (weak) id<NSSharingServicePickerToolbarItemDelegate> delegate;
      procedure setDelegate(newValue: id); message 'setDelegate:';
      function delegate: id; message 'delegate';
   end;

   //NSSharingServicePickerDelegate
   NSSharingServicePickerToolbarItemDelegateProtocol = objcprotocol external name 'NSSharingServicePickerToolbarItemDelegate' (NSObjectProtocol)
   optional
      // (NSArray *)itemsForSharingServicePickerToolbarItem:(NSSharingServicePickerToolbarItem *)pickerToolbarItem NS_SWIFT_UI_ACTOR;
      function itemsForSharingServicePickerToolbarItem(pickerToolbarItem: NSSharingServicePickerToolbarItem): NSArray; message 'itemsForSharingServicePickerToolbarItem:';
   end;
   {$ENDIF}

   {

     @see https://developer.apple.com/documentation/appkit/nsmenutoolbaritem?language=objc
   }
   NSMenuToolbarItem = objcclass external (NSToolbarItem)
   public
      procedure setMenu(newValue: NSMenu); message 'setMenu:';
      function menu: NSMenu; message 'menu';
   end;

   {
     An attempt to add functions not available in Lazarus yet.
   }
   NSToolbar_NSFuture = objccategory external name 'NSFuture' (NSToolbar)
      {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1300}
      procedure setCenteredItemIdentifiers(newValue: NSSet); message 'setCenteredItemIdentifiers:';
      function centeredItemIdentifiers: NSSet message 'centeredItemIdentifiers';
      {$ENDIF}
   end;

{$ENDIF LCLCocoa}

implementation

end.

