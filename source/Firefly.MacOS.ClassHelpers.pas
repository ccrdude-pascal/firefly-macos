unit Firefly.MacOS.ClassHelpers;

{$mode Delphi}{$H+}
{$modeswitch objectivec1}
{$modeswitch typehelpers}

interface

uses
   Classes,
   SysUtils,
   Graphics,
   NSHelpers,
   CocoaAll;

type

   { StringCocoaHelper }

   StringCocoaHelper = type helper for string
      function ToNSString: NSString;
      procedure FromNSString(AString: NSString);
   end;

   { TMemoryStreamCocoaHelper }

   TMemoryStreamCocoaHelper = class helper for TMemoryStream
      function ToNSData: NSData;
      procedure FromNSData(TheData: NSData);
   end;

   { TBitmapCocoaHelper }

   TBitmapCocoaHelper = class helper for TBitmap
      function ToNSImage: NSImage;
   end;

   { TStringListCocoaHelper }

   TStringListCocoaHelper = class helper for TStringList
      function ToNSArray: NSArray;
   end;

implementation

{ StringCocoaHelper }

function StringCocoaHelper.ToNSString: NSString;
begin
   Result := NSSTR(Self);
end;

procedure StringCocoaHelper.FromNSString(AString: NSString);
begin
   Self := NSStrToStr(AString);
end;

{ TStreamHelper }

function TMemoryStreamCocoaHelper.ToNSData: NSData;
begin
   Result := NSData.dataWithBytes_length(Self.Memory, Self.Size);
end;

procedure TMemoryStreamCocoaHelper.FromNSData(TheData: NSData);
begin
   // untested!
   Self.SetSize(0);
   Self.Write(TheData.bytes^, TheData.length);
end;

{ TBitmapCocoaHelper }

function TBitmapCocoaHelper.ToNSImage: NSImage;
var
   ms: TMemoryStream;
begin
   ms := TMemoryStream.Create;
   try
      Self.SaveToStream(ms);
      Result := NSImage.alloc.initWithData(ms.ToNSData);
   finally
      ms.Free;
   end;
end;

{ TStringListCocoaHelper }

function TStringListCocoaHelper.ToNSArray: NSArray;
var
   a: array of NSString = [];
   i: integer = 0;
   s: string;
begin
  if (0 = Self.Count) then begin
     Result := NSArray.alloc.init;
     Exit;
  end;
   SetLength(a, Self.Count);
   for s in Self do begin
      a[i] := NSSTR(s);
      Inc(i);
   end;
   Result := NSArray.arrayWithObjects_count(idptr(@a[0]), Length(a));
end;

end.
