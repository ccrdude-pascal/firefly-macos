{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Imports LCL TToolbar into native macOS toolbar.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// Uses code from Rhyt @ https://www.lazarusforum.de/viewtopic.php?f=23&t=15347
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-02-08  pk  15m  Draft.
// *****************************************************************************
   )
}
unit Firefly.MacOS.Toolbar.LCLImporter;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Controls,
   ComCtrls,
   Firefly.MacOS.Toolbar,
   Firefly.MacOS.Toolbar.Items;

type

   { TMacToolbarFromLCLToolbar }

   TMacToolbarFromLCLToolbar = class(TMacToolbar)
   private
      FLCLToolbar: TToolBar;
   protected
      function FindToolbar: TToolBar;
      procedure ImportFromToolbar(AToolbar: TToolbar);
   public
      constructor Create(AOwner: TComponent); override;
   published
      property LCLToolbar: TToolBar read FLCLToolbar write FLCLToolbar;
   end;

implementation


{ TMacToolbarFromLCLToolbar }

function TMacToolbarFromLCLToolbar.FindToolbar: TToolBar;
var
   i: integer;
   c: TWinControl;

begin
   Result := FLCLToolbar;
   if Assigned(Result) then begin
      Exit;
   end;
   if (not (Self.Owner is TWinControl)) then begin
      Exit;
   end;
   c := TWinControl(Self.Owner);
   for i := 0 to Pred(c.ControlCount) do begin
      if c.Controls[i] is TToolbar then begin
         Result := TToolbar(c.Controls[i]);
         Exit;
      end;
   end;
end;

procedure TMacToolbarFromLCLToolbar.ImportFromToolbar(AToolbar: TToolbar);
var
   b: TToolButton;
   item: TMacToolbarItem;
   i: integer;
begin
   Self.ToolbarIdentifier := AToolbar.Name;
   Self.ImageList := AToolbar.Images;
   if Assigned(AToolbar.Images) then begin
      if AToolbar.ShowCaptions then begin
         Self.DisplayMode := mtdmIconAndLabel;
      end else begin
         Self.DisplayMode := mtdmIconOnly;
      end;
   end else begin
      Self.DisplayMode := mdtmLabelOnly;
   end;
   for i := 0 to Pred(AToolbar.ButtonCount) do begin
      b := AToolbar.Buttons[i];
      if (b.Style in [tbsButtonDrop, tbsDropdown]) then begin
         item := Self.Items.AddItem(TMacToolbarMenuItem);
         TMacToolbarMenuItem(item).DropDownMenu := b.DropdownMenu;
      end else begin
         item := Self.Items.AddItem(TMacToolbarItem);
      end;
      item.Identifier := b.Name;
      item.Caption := b.Caption;
      item.Hint := b.Hint;
      item.ImageIndex := b.ImageIndex;
      item.Action := b.Action;
   end;
end;

constructor TMacToolbarFromLCLToolbar.Create(AOwner: TComponent);
var
   tb: TToolbar;
begin
   inherited Create(AOwner);
   tb := FindToolbar;
   if Assigned(tb) then begin
      ImportFromToolbar(tb);
      if Self.Attach then begin
         tb.Visible := false;
      end;
   end;
end;
end.
