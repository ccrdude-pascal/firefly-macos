{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(macOS toolbar itemss.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
   )
}    unit Firefly.MacOS.Toolbar.Items;

{$mode Delphi}{$H+}
{$modeswitch objectivec1}

interface

uses
   Classes,
   LCLClasses,
   SysUtils,
   Forms,
   ActnList,
   Menus,
   Graphics,
   ImgList,
   Controls,
   Dialogs,
   Generics.Collections,
   // now macOS specific
   CocoaAll,
   // now own code
   Firefly.MacOS.ClassHelpers,
   Firefly.MacOS.Toolbar.Headers;

type
   TOnGetSharedStringsEvent = procedure(AList: TStrings) of object;

   TMacToolbarItemImageType = (mtiitNone, mtiitDefault, mtiitFile, mtiitImageList);
   TMacToolbarItemType = (mtitItem, mtitItemGroup, mtitSearchItem, mtitMenuItem, mtitShareItem);
   TMacSegmentStyle = ( // .
      mssAutomatic = 0, // .
      mssRounded = 1, // .
      mssTexturedRounded = 2, // macOS 10.5 +
      mssRoundRect = 3, // .
      mssTexturedSquare = 4, // .
      mssCapsule = 5, // macOS 10.5 +
      mssSmallSquare = 6, // .
      mssSeparated = 8 // macOS 10.10 +
      );

   TMacToolbarItem = class;
   TMacToolbarItemClass = class of TMacToolbarItem;
   TMacToolbarItemList = class;

   { TMacToolbarItemActionLink }

   TMacToolbarItemActionLink = class(TActionLink)
   protected
      FClient: TMacToolbarItem;
      procedure AssignClient(AClient: TObject); override;
   protected
      procedure SetCaption(const Value: string); override;
      procedure SetEnabled(Value: boolean); override;
      procedure SetHelpContext({%H-}Value: THelpContext); override;
      procedure SetHint(const Value: string); override;
      procedure SetImageIndex(Value: integer); override;
      procedure SetVisible(Value: boolean); override;
      procedure SetOnExecute(Value: TNotifyEvent); override;
   public
      function IsCaptionLinked: boolean; override;
      function IsEnabledLinked: boolean; override;
      function IsHelpContextLinked: boolean; override;
      function IsHintLinked: boolean; override;
      function IsImageIndexLinked: boolean; override;
      function IsVisibleLinked: boolean; override;
   end;

   TMacToolbarItemActionLinkClass = class of TMacToolbarItemActionLink;

   { TMacToolbarItem }

   TMacToolbarItem = class(TLCLComponent)
   private
      FImageIndex: integer;
      FList: TMacToolbarItemList;
      FActionLink: TMacToolbarItemActionLink;
      FCaption: string;
      FEnabled: boolean;
      FHint: string;
      FImageDefaulType: NSString;
      FImageFilename: string;
      FImageType: TMacToolbarItemImageType;
      FIdentifier: string;
      FIsCentered: boolean;
      FIsDefault: boolean;
      FIsImmovable: boolean;
      FIsSelectable: boolean;
      FItemType: TMacToolbarItemType;
      FSubItems: TMacToolbarItemList;
      function GetAction: TBasicAction;
      procedure SetAction(AValue: TBasicAction);
      procedure SetImageDefaulType(AValue: NSString);
      procedure SetImageFilename(AValue: string);
      procedure DoActionChange(Sender: TObject);
      procedure SetImageIndex(AValue: integer);
   protected
      FMacToolbarItem: NSToolbarItem;
      procedure AssignItemImage(AnItem: NSToolbarItem);
      procedure AssignTo(Dest: TPersistent); override;
      function GetActionLinkClass: TMacToolbarItemActionLinkClass; virtual;
      procedure ActionChange(Sender: TObject; CheckDefaults: boolean); virtual;
      property ActionLink: TMacToolbarItemActionLink read FActionLink write FActionLink;
   public
      constructor Create(TheOwner: TMacToolbarItemList); virtual; overload;
      destructor Destroy; override;
      function GetItem(ADelegate: id): NSToolbarItem;
      property ImageType: TMacToolbarItemImageType read FImageType write FImageType;
      property ImageDefaulType: NSString read FImageDefaulType write SetImageDefaulType;
      property ItemType: TMacToolbarItemType read FItemType write FItemType;
      property Owner: TMacToolbarItemList read FList;
   published
      property Identifier: string read FIdentifier write FIdentifier;
      property Caption: string read FCaption write FCaption;
      property Hint: string read FHint write FHint;
      property Action: TBasicAction read GetAction write SetAction;
      property ImageFilename: string read FImageFilename write SetImageFilename;
      property Enabled: boolean read FEnabled write FEnabled;
      property ImageIndex: integer read FImageIndex write SetImageIndex;
      property IsDefault: boolean read FIsDefault write FIsDefault;
      property IsImmovable: boolean read FIsImmovable write FIsImmovable;
      property IsSelectable: boolean read FIsSelectable write FIsSelectable;
      property IsCentered: boolean read FIsCentered write FIsCentered;
      property SubItems: TMacToolbarItemList read FSubItems;
   end;

   { TMacToolbarItemGroup }

   TMacToolbarItemGroup = class(TMacToolbarItem)
   private
      FSegmentStyle: TMacSegmentStyle;
   protected
      FSegmentedControl: NSSegmentedControl;
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create(TheOwner: TMacToolbarItemList); override;
      function GetItemGroup(ADelegate: id): NSToolbarItemGroup;
      property SegmentStyle: TMacSegmentStyle read FSegmentStyle write FSegmentStyle;
      property SegmentedControl: NSSegmentedControl read FSegmentedControl;
   end;

   { TMacToolbarSearchItem }

   TMacToolbarSearchItem = class(TMacToolbarItem)
   private
      FOnBeginSearch: TNotifyEvent;
      FOnChange: TNotifyEvent;
      FOnEndSearch: TNotifyEvent;
       {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
      FSearchItem: NSSearchToolbarItem;
       {$ENDIF}
      function GetSearchText: string;
      procedure SetSearchText(AValue: string);
   protected
      procedure AssignTo(Dest: TPersistent); override;
      {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
      property SearchItem: NSSearchToolbarItem read FSearchItem;
      {$ENDIF}
   public
      constructor Create(TheOwner: TMacToolbarItemList); override;
      {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
      function GetSearchItem(ADelegate: id): NSSearchToolbarItem;
      {$ENDIF}
      property SearchText: string read GetSearchText write SetSearchText;
      property OnBeginSearch: TNotifyEvent read FOnBeginSearch write FOnBeginSearch;
      property OnEndSearch: TNotifyEvent read FOnEndSearch write FOnEndSearch;
      property OnChange: TNotifyEvent read FOnChange write FOnChange;
   end;

   { TMacToolbarMenuItem }

   TMacToolbarMenuItem = class(TMacToolbarItem)
   private
      FDropDownMenu: TPopupmenu;
   protected
      FMenuItem: NSMenuToolbarItem;
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create(TheOwner: TMacToolbarItemList); override;
      function GetMenuItem(ADelegate: id): NSMenuToolbarItem;
      property DropDownMenu: TPopupmenu read FDropDownMenu write FDropDownMenu;
   end;

   { TMacToolbarShareItem }

   TMacToolbarShareItem = class(TMacToolbarItem)
   private
      FOnGetSharedStrings: TOnGetSharedStringsEvent;
      FShareItem: NSSharingServicePickerToolbarItem;
   protected
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create(TheOwner: TMacToolbarItemList); override;
      function GetShareItem(ADelegate: id): NSSharingServicePickerToolbarItem;
      property OnGetSharedStrings: TOnGetSharedStringsEvent read FOnGetSharedStrings write FOnGetSharedStrings;
   end;

   { TMacToolbarItemList }

   TMacToolbarItemList = class(TObjectList<TMacToolbarItem>)
   private
      FImageList: TCustomImageList;
      procedure SetImageList(AValue: TCustomImageList);
   protected
      FMacToolbar: NSToolbar;
      procedure GetAllowedItems(AList: TStringList);
      procedure GetDefaultItems(AList: TStringList);
      procedure GetImmovableItems(AList: TStringList);
      procedure GetSelectableItems(AList: TStringList);
      function DoRemove(AIndex: SizeInt; ACollectionNotification: TCollectionNotification): TMacToolbarItem; override;
   public
      function FindItemWithIdentifier(AnIdentifier: NSString; ARecursive: boolean = False): TMacToolbarItem;
      function FindItemWithSearchField(AField: NSSearchField; ARecursive: boolean = False): TMacToolbarSearchItem;
      function FindItemWithObject(AnObject: id; ARecursive: boolean = True): TMacToolbarItem;
      function AddItem: TMacToolbarItem; overload;
      function AddItem(AClass: TMacToolbarItemClass): TMacToolbarItem; overload;
      function AddItem(AnIdentifier: string; AnAction: TAction): TMacToolbarItem; overload;
      function AddItem(AnIdentifier, ACaption, AHint: string; AnAction: TAction): TMacToolbarItem; overload;
      function AddItemAfter(ABefore: TMacToolbarItem): TMacToolbarItem;
      function AddItemGroup: TMacToolbarItemGroup; overload;
      function AddItemGroup(AnIdentifier: string): TMacToolbarItemGroup; overload;
      {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
      function AddSearchItem: TMacToolbarSearchItem; overload;
      function AddMenuItem: TMacToolbarMenuItem; overload;
      {$ENDIF}
      procedure Assign(Source: TMacToolbarItemList);
      function GetAllowedItemsArray: NSArray;
      function GetDefaultItemsArray: NSArray;
      function GetImmovableItemsArray: NSArray;
      function GetSelectableItemsArray: NSArray;
      function GetCenteredItemsArray: NSArray;
      property MacToolbar: NSToolbar read FMacToolbar write FMacToolbar;
      property ImageList: TCustomImageList read FImageList write SetImageList;
   end;

   { TMacToolbarItems }

   TMacToolbarItems = class(TPersistent)
   private
      FImageList: TCustomImageList;
      FMacToolbar: NSToolbar;
      function GetCount: integer;
      function GetItemFromIndex(Index: Integer): TMacToolbarItem;
      procedure ReadData(Stream: TStream);
      procedure SetImageList(AValue: TCustomImageList);
      procedure SetMacToolbar(AValue: NSToolbar);
      procedure WriteData(Stream: TStream); overload;
      procedure WriteData(Stream: TStream; WriteDataPointer: boolean); overload;
   protected
      FList: TMacToolbarItemList;
      procedure DefineProperties(Filer: TFiler); override;
   public
      constructor Create;
      destructor Destroy; override;
      property MacToolbar: NSToolbar read FMacToolbar write SetMacToolbar;
      property ImageList: TCustomImageList read FImageList write SetImageList;
      property Item[Index: Integer]: TMacToolbarItem read GetItemFromIndex; default;
   published
      property Count: integer read GetCount;
   end;

implementation


{ TMacToolbarItem }

procedure TMacToolbarItem.SetImageFilename(AValue: string);
begin
   FImageFilename := AValue;
   FImageType := mtiitFile;
end;

procedure TMacToolbarItem.DoActionChange(Sender: TObject);
begin
   if Sender = Action then begin
      ActionChange(Sender, False);
   end;
end;

procedure TMacToolbarItem.SetImageIndex(AValue: integer);
begin
   FImageIndex := AValue;
   FImageType := mtiitImageList;
end;

procedure TMacToolbarItem.AssignTo(Dest: TPersistent);
var
   item: TMacToolbarItem;
begin
   if Dest is TMacToolbarItem then begin
      item := TMacToolbarItem(Dest);
      // skip FList!
      item.FActionLink := Self.FActionLink;
      item.FCaption := Self.FCaption;
      item.FImageIndex := Self.FImageIndex;
      item.FEnabled := Self.FEnabled;
      item.FHint := Self.FHint;
      item.FImageDefaulType := Self.FImageDefaulType;
      item.FImageFilename := Self.FImageFilename;
      item.FImageType := Self.FImageType;
      item.FImageIndex := Self.FImageIndex;
      item.FIdentifier := Self.FIdentifier;
      item.FIsCentered := Self.FIsCentered;
      item.FIsDefault := Self.FIsDefault;
      item.FIsImmovable := Self.FIsImmovable;
      item.FIsSelectable := Self.FIsSelectable;
      item.FItemType := Self.FItemType;
      item.FSubItems.Assign(Self.FSubItems);
   end;
end;

procedure TMacToolbarItem.AssignItemImage(AnItem: NSToolbarItem);
var
   bmp: TBitmap;
   img: NSImage;
   sFilename: string;
begin
   case Self.ImageType of
      mtiitDefault: begin
         AnItem.setImage(NSImage.imageNamed(Self.ImageDefaulType));
      end;
      mtiitFile: begin
         sFilename := ExtractFilePath(Application.ExeName) + '../' + Self.ImageFilename;
         if FileExists(sFilename) then begin
            img := NSImage.alloc;
            img.initWithContentsOfFile(NSSTR(sFilename));
            AnItem.setImage(img);
         end else begin
            // TODO : error reporting
            // ShowMessage('Unable to assign image ' + ExtractFilePath(Application.ExeName) + '../' + Self.ImageFilename);
         end;
      end;
      mtiitImageList: begin
         if Assigned(Self.FList.FImageList) and (Self.ImageIndex > -1) then begin
            bmp := TBitmap.Create;
            try
               Self.FList.FImageList.GetBitmap(Self.ImageIndex, bmp);
               AnItem.setImage(bmp.ToNSImage);
            finally
               bmp.Free;
            end;
         end;
      end;
      else
      begin
         // no image to be set
      end;
   end;
end;

function TMacToolbarItem.GetItem(ADelegate: id): NSToolbarItem;
begin
   Result := NSToolbarItem.alloc.initWithItemIdentifier(NSSTR(Self.Identifier));
   Result.setLabel(NSSTR(Self.Caption));
   Result.setToolTip(NSSTR(Self.Hint));
   Result.setTarget(ADelegate);
   Result.setAction(sel_getUid('toolbarItemClick:'));
   AssignItemImage(Result);
   Self.FMacToolbarItem := Result;
end;

function TMacToolbarItem.GetActionLinkClass: TMacToolbarItemActionLinkClass;
begin
   Result := TMacToolbarItemActionLink;
end;

procedure TMacToolbarItem.ActionChange(Sender: TObject; CheckDefaults: boolean);
var
   NewAction: TCustomAction;
begin
   if Sender is TCustomAction then begin
      NewAction := TCustomAction(Sender);
      //if (not CheckDefaults) or (AutoCheck = False) then begin
      //   AutoCheck := NewAction.AutoCheck;
      //end;
      if (not CheckDefaults) or (Caption = '') then begin
         Caption := NewAction.Caption;
      end;
      //if (not CheckDefaults) or (Checked = False) then begin
      //   Checked := NewAction.Checked;
      //end;
      if (not CheckDefaults) or (Enabled = True) then begin
         Enabled := NewAction.Enabled;
      end;
      //if (not CheckDefaults) or (HelpContext = 0) then begin
      //   HelpContext := NewAction.HelpContext;
      //end;
      if (not CheckDefaults) or (Hint = '') then begin
         Hint := NewAction.Hint;
      end;
      //if RadioItem and (not CheckDefaults or (GroupIndex = 0)) then begin
      //   GroupIndex := NewAction.GroupIndex;
      //end;
      if (not CheckDefaults) or (ImageIndex = -1) then begin
         ImageIndex := NewAction.ImageIndex;
      end;
      //if (not CheckDefaults) or (ShortCut = scNone) then begin
      //   ShortCut := NewAction.ShortCut;
      //end;
      //if (not CheckDefaults) or (Visible = True) then begin
      //   Visible := NewAction.Visible;
      //end;
   end;
end;

procedure TMacToolbarItem.SetImageDefaulType(AValue: NSString);
begin
   FImageDefaulType := AValue;
   FImageType := mtiitDefault;
end;

function TMacToolbarItem.GetAction: TBasicAction;
begin
   if Assigned(FActionLink) then begin
      Result := FActionLink.Action;
   end else begin
      Result := nil;
   end;
end;

procedure TMacToolbarItem.SetAction(AValue: TBasicAction);
begin
   if AValue = nil then begin
      FActionLink.Free;
      FActionLink := nil;
   end else begin
      if FActionLink = nil then begin
         FActionLink := GetActionLinkClass.Create(Self);
      end;
      FActionLink.Action := AValue;
      FActionLink.OnChange := DoActionChange;
      ActionChange(AValue, csLoading in AValue.ComponentState);
      AValue.FreeNotification(Self);
   end;
end;

constructor TMacToolbarItem.Create(TheOwner: TMacToolbarItemList);
begin
   inherited Create(nil);
   FList := TheOwner;
   FImageType := mtiitNone;
   FIsDefault := True;
   FImageIndex := -1;
   FIsImmovable := False;
   FIsSelectable := False;
   FIsCentered := False;
   FEnabled := True;
   FSubItems := TMacToolbarItemList.Create;
   FItemType := mtitItem;
end;

destructor TMacToolbarItem.Destroy;
begin
   FSubItems.Free;
   inherited Destroy;
end;

{ TMacToolbarItemActionLink }

procedure TMacToolbarItemActionLink.AssignClient(AClient: TObject);
begin
   FClient := AClient as TMacToolbarItem;
end;

procedure TMacToolbarItemActionLink.SetCaption(const Value: string);
begin
   if IsCaptionLinked then begin
      FClient.Caption := Value;
   end;
end;

procedure TMacToolbarItemActionLink.SetEnabled(Value: boolean);
begin
   if IsEnabledLinked then begin
      FClient.Enabled := Value;
   end;
end;

procedure TMacToolbarItemActionLink.SetHelpContext(Value: THelpContext);
begin
   if IsHelpContextLinked then begin
      // FClient.HelpContext := Value;
   end;
end;

procedure TMacToolbarItemActionLink.SetHint(const Value: string);
begin
   if IsHintLinked then begin
      FClient.Hint := Value;
   end;
end;

procedure TMacToolbarItemActionLink.SetImageIndex(Value: integer);
begin
   if IsImageIndexLinked then begin
      FClient.ImageIndex := Value;
   end;
end;

procedure TMacToolbarItemActionLink.SetVisible(Value: boolean);
begin
   if IsVisibleLinked then begin
      //FClient.Visible := Value;
   end;
end;

procedure TMacToolbarItemActionLink.SetOnExecute(Value: TNotifyEvent);
begin
   inherited;
end;

function TMacToolbarItemActionLink.IsCaptionLinked: boolean;
begin
   Result := inherited IsCaptionLinked and (AnsiCompareText(FClient.Caption, (Action as TCustomAction).Caption) = 0);
end;

function TMacToolbarItemActionLink.IsEnabledLinked: boolean;
begin
   Result := inherited IsEnabledLinked and (FClient.Enabled = (Action as TCustomAction).Enabled);
end;

function TMacToolbarItemActionLink.IsHelpContextLinked: boolean;
begin
   Result := inherited IsHelpContextLinked;
   //             and (FClient.HelpContext = (Action as TCustomAction).HelpContext);
end;

function TMacToolbarItemActionLink.IsHintLinked: boolean;
begin
   Result := inherited IsHintLinked and (FClient.Hint = (Action as TCustomAction).Hint);
end;

function TMacToolbarItemActionLink.IsImageIndexLinked: boolean;
begin
   Result := inherited IsImageIndexLinked and (FClient.ImageIndex = (Action as TCustomAction).ImageIndex);
end;

function TMacToolbarItemActionLink.IsVisibleLinked: boolean;
begin
   Result := inherited IsVisibleLinked;
   //             and (FClient.Visible = (Action as TCustomAction).Visible);
end;

{ TMacToolbarItemGroup }

procedure TMacToolbarItemGroup.AssignTo(Dest: TPersistent);
var
   item: TMacToolbarItemGroup;
begin
   inherited AssignTo(Dest);
   if Dest is TMacToolbarItemGroup then begin
      inherited AssignTo(Dest);
      item := TMacToolbarItemGroup(Dest);
      item.FSegmentedControl := Self.FSegmentedControl;
      item.FSegmentStyle := Self.FSegmentStyle;
   end;
end;

function TMacToolbarItemGroup.GetItemGroup(ADelegate: id): NSToolbarItemGroup;
var
   a: array of NSToolbarItem = [];
   i: integer = 0;
   item: TMacToolbarItem;
   nsa: NSArray;
begin
   Result := NSToolbarItemGroup.alloc.initWithItemIdentifier(NSSTR(Self.Identifier));
   Result.setLabel(NSSTR(Self.Caption));
   Result.setTarget(ADelegate);
   Result.setAction(sel_getUid('toolbarItemGroupClick:'));
   SetLength(a, Self.SubItems.Count);
   for item in Self.SubItems do begin
      a[i] := item.GetItem(ADelegate);
      Inc(i);
   end;
   nsa := NSArray.arrayWithObjects_count(idptr(@a[0]), Length(a));
   Result.setSubitems(nsa);

   FSegmentedControl := (NSSegmentedControl.alloc).init;
   FSegmentedControl.setSegmentCount(Length(a));
   for i := 0 to Pred(Length(a)) do begin
      FSegmentedControl.setWidth_forSegment(40, i);
      FSegmentedControl.setImage_forSegment(a[i].image, i);
      // FSegmentedControl.setMenu_forSegment(NSMenu(Self.SubItems[i].DropDownMenu.Handle), i);
      // FSegmentedControl.setLabel_forSegment(a[i].label_, i);
   end;
   // FSegmentedControl.setMenu(NSMenu(Self.SubItems[0].DropDownMenu.Handle));
   FSegmentedControl.setSegmentStyle(NSSegmentStyle(FSegmentStyle));
   FSegmentedControl.setTarget(ADelegate);
   FSegmentedControl.setAction(sel_getUid('segmentedControlClick:'));
   Result.setView(FSegmentedControl);
end;

constructor TMacToolbarItemGroup.Create(TheOwner: TMacToolbarItemList);
begin
   inherited;
   FSegmentStyle := mssAutomatic;
   FItemType := mtitItemGroup;
end;

{ TMacToolbarSearchItem }

function TMacToolbarSearchItem.GetSearchText: string;
begin
   Result := '';
   {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
   Result.FromNSString(NSString(FSearchItem.searchField.stringValue));
   {$ENDIF}
end;

procedure TMacToolbarSearchItem.SetSearchText(AValue: string);
begin
   {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
   FSearchItem.searchField.setStringValue(NSStr(AValue));
   {$ENDIF}
end;

procedure TMacToolbarSearchItem.AssignTo(Dest: TPersistent);
var
   item: TMacToolbarSearchItem;
begin
   if Dest is TMacToolbarSearchItem then begin
      inherited AssignTo(Dest);
      item := TMacToolbarSearchItem(Dest);
      {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
      item.FSearchItem := Self.FSearchItem;
      {$ENDIF}
   end;
end;

{$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
function TMacToolbarSearchItem.GetSearchItem(ADelegate: id): NSSearchToolbarItem;
begin
   Result := NSSearchToolbarItem.alloc.initWithItemIdentifier(NSSTR(Self.Identifier));
   //Result.setTarget(ADelegate);
   //Result.setAction(sel_getUid('toolbarSearchItemClick:'));
   Result.searchField.setPlaceholderString(NSSTR(Self.Hint));
   Result.searchField.setDelegate(ADelegate);
   Self.FSearchItem := Result;
end;
{$ENDIF}

constructor TMacToolbarSearchItem.Create(TheOwner: TMacToolbarItemList);
begin
   inherited Create(TheOwner);
   FItemType := mtitSearchItem;
   Self.Identifier := 'searchItem';
end;

{ TMacToolbarMenuItem }

procedure TMacToolbarMenuItem.AssignTo(Dest: TPersistent);
var
   item: TMacToolbarMenuItem;
begin
   if Dest is TMacToolbarMenuItem then begin
      inherited AssignTo(Dest);
      item := TMacToolbarMenuItem(Dest);
      item.FMenuItem := Self.FMenuItem;
      item.FDropDownMenu := Self.FDropDownMenu;
   end;
end;

function TMacToolbarMenuItem.GetMenuItem(ADelegate: id): NSMenuToolbarItem;
begin
   Result := NSMenuToolbarItem.alloc.initWithItemIdentifier(NSSTR(Self.Identifier));
   Self.FMenuItem := Result;
   Result.setLabel(NSSTR(Self.Caption));
   Result.setToolTip(NSSTR(Self.Hint));
   if Assigned(FDropDownMenu) then begin
      // @see https://developer.apple.com/forums/thread/656153
      Result.setMenu(NSMenu(FDropDownMenu.Handle));
   end;
   AssignItemImage(Result);
end;

constructor TMacToolbarMenuItem.Create(TheOwner: TMacToolbarItemList);
begin
   inherited Create(TheOwner);
   FDropDownMenu := nil;
   FItemType := mtitMenuItem;
end;

{ TMacToolbarShareItem }

procedure TMacToolbarShareItem.AssignTo(Dest: TPersistent);
var
   item: TMacToolbarShareItem;
begin
   if Dest is TMacToolbarShareItem then begin
      inherited AssignTo(Dest);
      item := TMacToolbarShareItem(Dest);
      item.FShareItem := Self.FShareItem;
   end;
end;

constructor TMacToolbarShareItem.Create(TheOwner: TMacToolbarItemList);
begin
   inherited Create(TheOwner);
   FShareItem := nil;
   FItemType := mtitShareItem;
   Self.Identifier := 'shareItem';
   Self.ImageDefaulType := NSImageNameShareTemplate;
end;

function TMacToolbarShareItem.GetShareItem(ADelegate: id): NSSharingServicePickerToolbarItem;
begin
   Result := NSSharingServicePickerToolbarItem.alloc.initWithItemIdentifier(NSSTR(Self.Identifier));
   Self.FShareItem := Result;
   Result.setDelegate(ADelegate);
   Result.setLabel(NSSTR(Self.Caption));
   Result.setToolTip(NSSTR(Self.Hint));
end;

{ TMacToolbarItemList }

function TMacToolbarItemList.FindItemWithIdentifier(AnIdentifier: NSString; ARecursive: boolean): TMacToolbarItem;
var
   i: TMacToolbarItem;
begin
   Result := nil;
   for i in Self do begin
      if 0 = AnIdentifier.compare(NSSTR(i.Identifier)) then begin
         Result := i;
         Exit;
      end;
      if ARecursive then begin
         Result := i.SubItems.FindItemWithIdentifier(AnIdentifier, True);
         if Assigned(Result) then begin
            Exit;
         end;
      end;
   end;
end;

function TMacToolbarItemList.FindItemWithSearchField(AField: NSSearchField; ARecursive: boolean): TMacToolbarSearchItem;
var
   i: TMacToolbarItem;
begin
   Result := nil;
   {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
   for i in Self do begin
      if i is TMacToolbarSearchItem then begin
         if TMacToolbarSearchItem(i).SearchItem.searchField = AField then begin
            Result := TMacToolbarSearchItem(i);
            Exit;
         end;
      end;
      if ARecursive then begin
         Result := i.SubItems.FindItemWithSearchField(AField, True);
         if Assigned(Result) then begin
            Exit;
         end;
      end;
   end;
   {$ENDIF}
end;

function TMacToolbarItemList.FindItemWithObject(AnObject: id; ARecursive: boolean): TMacToolbarItem;
var
   i: TMacToolbarItem;
begin
   Result := nil;
   for i in Self do begin
      if (i is TMacToolbarSearchItem) then begin
         {$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
         if (TMacToolbarSearchItem(i).SearchItem.searchField = AnObject) then begin
            Result := i;
            Exit;
         end else if (TMacToolbarSearchItem(i).SearchItem = AnObject) then begin
            Result := i;
            Exit;
         end;
         {$ENDIF}
      end;
      if ARecursive then begin
         Result := i.SubItems.FindItemWithObject(AnObject, True);
         if Assigned(Result) then begin
            Exit;
         end;
      end;
   end;
end;

function TMacToolbarItemList.AddItem: TMacToolbarItem;
begin
   Result := AddItem(TMacToolbarItem);
end;

function TMacToolbarItemList.AddItem(AnIdentifier, ACaption, AHint: string; AnAction: TAction): TMacToolbarItem;
begin
   Result := AddItem(AnIdentifier, AnAction);
   Result.Caption := ACaption;
   Result.Hint := AHint;
end;

function TMacToolbarItemList.AddItemAfter(ABefore: TMacToolbarItem): TMacToolbarItem;
begin
   Result := TMacToolbarItem.Create(Self);
   Self.Insert(Self.IndexOf(ABefore) + 1, Result);
end;

function TMacToolbarItemList.AddItem(AnIdentifier: string; AnAction: TAction): TMacToolbarItem;
begin
   Result := AddItem(TMacToolbarItem);
   Result.Identifier := AnIdentifier;
   Result.Action := AnAction;
end;

function TMacToolbarItemList.AddItem(AClass: TMacToolbarItemClass): TMacToolbarItem;
begin
   Result := AClass.Create(Self);
   Result.SubItems.FMacToolbar := FMacToolbar;
   Result.SubItems.ImageList := Self.ImageList;
   Self.Add(Result);
end;

function TMacToolbarItemList.AddItemGroup(AnIdentifier: string): TMacToolbarItemGroup;
begin
   Result := AddItemGroup;
   Result.Identifier := AnIdentifier;
end;

{$IF MAC_OS_X_VERSION_MIN_REQUIRED >= 1100}
function TMacToolbarItemList.AddSearchItem: TMacToolbarSearchItem;
begin
   Result := AddItem(TMacToolbarSearchItem) as TMacToolbarSearchItem;
end;

function TMacToolbarItemList.AddMenuItem: TMacToolbarMenuItem;
begin
   Result := AddItem(TMacToolbarMenuItem) as TMacToolbarMenuItem;
end;

{$ENDIF}

procedure TMacToolbarItemList.Assign(Source: TMacToolbarItemList);
var
   item: TMacToolbarItem;
   new: TMacToolbarItem;
begin
   Self.Clear;
   Self.ImageList := Source.ImageList;
   for item in Source do begin
      new := Self.AddItem(TMacToolbarItemClass(item.ClassType));
      new.Assign(item);
   end;
end;

function TMacToolbarItemList.AddItemGroup: TMacToolbarItemGroup;
begin
   Result := AddItem(TMacToolbarItemGroup) as TMacToolbarItemGroup;
end;

procedure TMacToolbarItemList.SetImageList(AValue: TCustomImageList);
var
   item: TMacToolbarItem;
begin
   FImageList := AValue;
   for item in Self do begin
      item.SubItems.ImageList := AValue;
   end;
end;

procedure TMacToolbarItemList.GetAllowedItems(AList: TStringList);
var
   item: TMacToolbarItem;
   sl: TStringList;
begin
   for item in Self do begin
      AList.Add(item.Identifier);
      if item.SubItems.Count > 0 then begin
         sl := TStringList.Create;
         try
            item.SubItems.GetAllowedItems(sl);
            AList.AddStrings(sl);
         finally
            sl.Free;
         end;
      end;
   end;
end;

{
  Returns an array of default items.
}
function TMacToolbarItemList.GetDefaultItemsArray: NSArray;
var
   sl: TStringList;
begin
   sl := TStringList.Create;
   try
      Self.GetDefaultItems(sl);
      Result := sl.ToNSArray;
   finally
      sl.Free;
   end;
end;

procedure TMacToolbarItemList.GetImmovableItems(AList: TStringList);
var
   item: TMacToolbarItem;
   sl: TStringList;
begin
   for item in Self do begin
      if item.IsImmovable then begin
         AList.Add(item.Identifier);
         if item.SubItems.Count > 0 then begin
            sl := TStringList.Create;
            try
               item.SubItems.GetImmovableItems(sl);
               AList.AddStrings(sl);
            finally
               sl.Free;
            end;
         end;
      end;
   end;
end;

procedure TMacToolbarItemList.GetSelectableItems(AList: TStringList);
var
   item: TMacToolbarItem;
   sl: TStringList;
begin
   for item in Self do begin
      if item.IsSelectable then begin
         AList.Add(item.Identifier);
         if item.SubItems.Count > 0 then begin
            sl := TStringList.Create;
            try
               item.SubItems.GetSelectableItems(sl);
               AList.AddStrings(sl);
            finally
               sl.Free;
            end;
         end;
      end;
   end;
end;

{
  Returns an array of allowed (all) items.
}
function TMacToolbarItemList.GetAllowedItemsArray: NSArray;
var
   sl: TStringList;
begin
   sl := TStringList.Create;
   try
      Self.GetAllowedItems(sl);
      Result := sl.ToNSArray;
   finally
      sl.Free;
   end;
end;

procedure TMacToolbarItemList.GetDefaultItems(AList: TStringList);
var
   item: TMacToolbarItem;
   sl: TStringList;
begin
   for item in Self do begin
      if item.IsDefault then begin
         AList.Add(item.Identifier);
         if item.SubItems.Count > 0 then begin
            sl := TStringList.Create;
            try
               item.SubItems.GetDefaultItems(sl);
               AList.AddStrings(sl);
            finally
               sl.Free;
            end;
         end;
      end;
   end;
end;

{
  Returns an array of immovable items.
}
function TMacToolbarItemList.GetImmovableItemsArray: NSArray;
var
   sl: TStringList;
begin
   sl := TStringList.Create;
   try
      Self.GetImmovableItems(sl);
      Result := sl.ToNSArray;
   finally
      sl.Free;
   end;
end;

{
  Returns an array of selectable items.
}
function TMacToolbarItemList.GetSelectableItemsArray: NSArray;
var
   sl: TStringList;
begin
   sl := TStringList.Create;
   try
      Self.GetSelectableItems(sl);
      Result := sl.ToNSArray;
   finally
      sl.Free;
   end;
end;

{
  Returns an array of items defined to be centered.

  This is compatible with macOS 13.0+

  The property centeredItemIdentifiers is Not defined in FreePascal yet.

  @see https://developer.apple.com/documentation/appkit/nstoolbar/3969238-centereditemidentifiers?language=objc
}
function TMacToolbarItemList.GetCenteredItemsArray: NSArray;
var
   item: TMacToolbarItem;
   a: array of NSString = [];
   i: integer = 0;
begin
   for item in Self do begin
      if item.IsCentered then begin
         SetLength(a, Succ(i));
         a[i] := NSSTR(item.Identifier);
         Inc(i);
      end;
   end;
   Result := NSArray.arrayWithObjects_count(idptr(@a[0]), Length(a));
end;

function TMacToolbarItemList.DoRemove(AIndex: SizeInt; ACollectionNotification: TCollectionNotification): TMacToolbarItem;
begin
   Result := inherited DoRemove(AIndex, ACollectionNotification);
   // TODO : if toolbar is live, remove from toolbar
   FMacToolbar.removeItemAtIndex(AIndex);
end;

{ TMacToolbarItems }

function TMacToolbarItems.GetCount: integer;
begin
   Result := FList.Count;
end;

function TMacToolbarItems.GetItemFromIndex(Index: Integer): TMacToolbarItem;
begin
   Result := FList[Index];
end;

procedure TMacToolbarItems.ReadData(Stream: TStream);
begin

end;

procedure TMacToolbarItems.SetImageList(AValue: TCustomImageList);
begin
   FImageList := AValue;
   FList.ImageList := AValue;
end;

procedure TMacToolbarItems.SetMacToolbar(AValue: NSToolbar);
begin
   FMacToolbar := AValue;
   FList.MacToolbar := AValue;
end;

procedure TMacToolbarItems.WriteData(Stream: TStream);
begin

end;

procedure TMacToolbarItems.WriteData(Stream: TStream; WriteDataPointer: boolean);
begin

end;

procedure TMacToolbarItems.DefineProperties(Filer: TFiler);

   function WriteNodes: boolean;
   var
      I: integer;
      Nodes: TMacToolbarItems;
   begin
      Nodes := TMacToolbarItems(Filer.Ancestor);
      if Nodes = nil then begin
         Result := Count > 0
      end
      else if Nodes.Count <> Count then begin
         Result := True;
      end else begin
         Result := False;
         for I := 0 to Count - 1 do begin
            //Result := not Item[I].IsEqual(Nodes[I]);
            Result := true;
            if Result then begin
               Break;
            end;
         end;
      end;
   end;

begin
   inherited DefineProperties(Filer);
   Filer.DefineBinaryProperty('Data', ReadData, WriteData, WriteNodes);
end;

constructor TMacToolbarItems.Create;
begin
   FList := TMacToolbarItemList.Create;
end;

destructor TMacToolbarItems.Destroy;
begin
   FList.Free;
   inherited Destroy;
end;

end.
