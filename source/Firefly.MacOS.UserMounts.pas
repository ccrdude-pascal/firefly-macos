{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Code to mount remote drives using SSH.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-24  pk   5m  Added header.
// *****************************************************************************
   )
}
unit Firefly.MacOS.UserMounts;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   IniFiles,
   Dialogs,
   Process,
   Firefly.Process,
   OVM.Attributes,
   Generics.Collections;

type
   TParameterArray = array of string;

   TOnProcessActionEvent = procedure(const AnExecutableName: TProcessString; const TheParameters: array of TProcessString;
      const TheOutput, TheErrors: string; TheExitStatus: integer) of object;

   TMacOSMountPoints = class;

   { TMacOSMountPoint }

   TMacOSMountPoint = class
   private
      FList: TMacOSMountPoints;
      FFolderName: string;
      FIsMounted: boolean;
      FKeyFilename: string;
      FOnAction: TOnProcessActionEvent;
      FRemoteHost: string;
      FRemotePath: string;
      FRemoteUsername: string;
      FVolumeName: string;
      procedure DoOnRunCommandEvent({%H-}Sender, {%H-}Context: TObject; Status: TRunCommandEventCode; const Message: string);
      function RunSSHFSCommand(const commands: array of TProcessString; out outputstring: string; Options: TProcessOptions = [];
         SWOptions: TShowWindowOptions = swoShow): boolean;
      function RunUnMountCommand(APath: string): boolean;
   public
      procedure LoadFromSection(AnIni: TCustomIniFile; ASectionName: string);
      procedure SaveToSection(AnIni: TCustomIniFile; ASectionName: string);
      function GetSSHFSParameters(AVolumesFolder: string = ''): string;
      function GetSSHFSParametersArray(AVolumesFolder: string = ''): TParameterArray;
      function IsMounted: boolean;
      procedure Unmount({%H-}Sender: TObject);
      procedure Mount({%H-}Sender: TObject);
   published
      property FolderName: string read FFolderName write FFolderName;
      property KeyFilename: string read FKeyFilename write FKeyFilename;
      property RemoteUsername: string read FRemoteUsername write FRemoteUsername;
      property RemoteHost: string read FRemoteHost write FRemoteHost;
      property RemotePath: string read FRemotePath write FRemotePath;
      property VolumeName: string read FVolumeName write FVolumeName;
      [AOVMBooleanImageIndex(0, -1)]
      property Mounted: boolean read FIsMounted;
      property OnAction: TOnProcessActionEvent read FOnAction write FOnAction;
   end;

   { TMacOSMountPoints }

   TMacOSMountPoints = class(TObjectList<TMacOSMountPoint>)
   private
      FFilename: string;
      FOnAction: TOnProcessActionEvent;
   protected
      procedure DoProcessAction(const AnExecutableName: TProcessString; const TheParameters: array of TProcessString;
         const TheOutput, TheErrors: string; TheExitStatus: integer);
   public
      destructor Destroy; override;
      procedure LoadFromIniFile(AFilename: string); overload;
      procedure LoadFromIniFile; overload;
      procedure SaveToIniFile(AFilename: string); overload;
      procedure SaveToIniFile; overload;
      procedure SetupTest;
      procedure UpdateStatus;
      property OnAction: TOnProcessActionEvent read FOnAction write FOnAction;
   end;


implementation

{ TMacOSMountPoints }

procedure TMacOSMountPoints.DoProcessAction(const AnExecutableName: TProcessString; const TheParameters: array of TProcessString;
   const TheOutput, TheErrors: string; TheExitStatus: integer);
begin
   if Assigned(FOnAction) then begin
      FOnAction(AnExecutableName, TheParameters, TheOutput, TheErrors, TheExitStatus);
   end;
end;

destructor TMacOSMountPoints.Destroy;
begin
   SaveToIniFile;
   inherited Destroy;
end;

procedure TMacOSMountPoints.LoadFromIniFile(AFilename: string);
var
   mif: TMemIniFile;
   sl: TStringList;
   s: string;
   point: TMacOSMountPoint;
begin
   FFilename := AFilename;
   Self.Clear;
   mif := TMemIniFile.Create(AFilename);
   try
      sl := TStringList.Create;
      try
         mif.ReadSections(sl);
         for s in sl do begin
            point := TMacOSMountPoint.Create;
            point.LoadFromSection(mif, s);
            point.FList := Self;
            Self.Add(point);
         end;
      finally
         sl.Free;
      end;
   finally
      mif.Free;
   end;
end;

procedure TMacOSMountPoints.LoadFromIniFile;
begin
   LoadFromIniFile(IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(GetUserDir) + '.config') + 'usermountpoints.ini');
end;

procedure TMacOSMountPoints.SaveToIniFile(AFilename: string);
var
   mif: TMemIniFile;
   point: TMacOSMountPoint;
begin
   mif := TMemIniFile.Create(AFilename);
   try
      for point in Self do begin
         point.SaveToSection(mif, point.VolumeName);
      end;
      mif.UpdateFile;
   finally
      mif.Free;
   end;
end;

procedure TMacOSMountPoints.SaveToIniFile;
begin
   SaveToIniFile(FFilename);
end;

procedure TMacOSMountPoints.SetupTest;
var
   mif: TMemIniFile;
   p: TMacOSMountPoint;
begin
   mif := TMemIniFile.Create('');
   try
      p := TMacOSMountPoint.Create;
      p.LoadFromSection(mif, '');
      Self.Add(p);
   finally
      mif.Free;
   end;
end;

procedure TMacOSMountPoints.UpdateStatus;
var
   sOut: string;
   mount: TMacOSMountPoint;
begin
   if RunCommand('mount', [], sOut) then begin
      for mount in Self do begin
         mount.FIsMounted := Pos(mount.FolderName, sOut) > 0;
      end;
   end;
end;

{ TMacOSMountPoint }

procedure TMacOSMountPoint.DoOnRunCommandEvent(Sender, Context: TObject; Status: TRunCommandEventCode; const Message: string);
begin
   if Status = RunCommandException then begin
      ShowMessage(Message);
   end;
end;

function TMacOSMountPoint.RunSSHFSCommand(const commands: array of TProcessString; out outputstring: string; Options: TProcessOptions;
   SWOptions: TShowWindowOptions): boolean;
const
   ForbiddenOptions = [poRunSuspended, poWaitOnExit];
var
   p: TProcess;
   i: integer;
   exitstatus: integer = 0;
   ErrorString: string;
   iResult: integer;
begin
   p := DefaultTProcess.Create(nil);
   if Options <> [] then begin
      P.Options := Options - ForbiddenOptions;
   end;
   p.OnRunCommandEvent := DoOnRunCommandEvent;
   P.ShowWindow := SwOptions;
   p.Executable := '/usr/local/bin/sshfs';
   if high(commands) >= 0 then for i := low(commands) to high(commands) do begin
         p.Parameters.add(commands[i]);
      end;
   try
      iResult := p.RunCommandLoop(outputstring, errorstring, exitstatus);
      Result := (iResult = 0);
      if Assigned(FOnAction) then begin
         FOnAction(p.Executable, Commands, outputstring, errorstring, exitstatus);
      end;
      TProcessExecutionSummaryList.Instance.AddFromProcess(p, outputstring, errorstring, exitstatus);
      FList.DoProcessAction(p.Executable, Commands, outputstring, errorstring, exitstatus);
   finally
      p.Free;
   end;
   if exitstatus <> 0 then begin
      Result := False;
   end;
end;

function TMacOSMountPoint.RunUnMountCommand(APath: string): boolean;
var
   p: TProcess;
   exitstatus: integer = 0;
   ErrorString: string;
   iResult: integer;
   outputstring: string;
begin
   p := DefaultTProcess.Create(nil);
   p.OnRunCommandEvent := DoOnRunCommandEvent;
   P.ShowWindow := swoShow;
   p.Executable := '/sbin/umount';
   p.Parameters.Add(APath);
   try
      iResult := p.RunCommandLoop(outputstring, errorstring, exitstatus);
      Result := (iResult = 0);
      if Assigned(FOnAction) then begin
         FOnAction(p.Executable, [APath], outputstring, errorstring, exitstatus);
      end;
      TProcessExecutionSummaryList.Instance.AddFromProcess(p, outputstring, errorstring, exitstatus);
      FList.DoProcessAction(p.Executable, [APath], outputstring, errorstring, exitstatus);
   finally
      p.Free;
   end;
   if exitstatus <> 0 then begin
      Result := False;
   end;
end;

procedure TMacOSMountPoint.LoadFromSection(AnIni: TCustomIniFile; ASectionName: string);
begin
   Self.FFolderName := AnIni.ReadString(ASectionName, 'FolderName', '~/Volumes/test');
   Self.FKeyFilename := AnIni.ReadString(ASectionName, 'KeyFilename', '~/.ssh/id_rsa');
   Self.FRemoteHost := AnIni.ReadString(ASectionName, 'RemoteHost', '');
   Self.FRemoteUsername := AnIni.ReadString(ASectionName, 'RemoteUsername', 'root');
   Self.FRemotePath := AnIni.ReadString(ASectionName, 'RemotePath', '/var/www');
   Self.FVolumeName := AnIni.ReadString(ASectionName, 'VolumeName', 'test');
end;

procedure TMacOSMountPoint.SaveToSection(AnIni: TCustomIniFile; ASectionName: string);
begin
   AnIni.WriteString(ASectionName, 'FolderName', Self.FFolderName);
   AnIni.WriteString(ASectionName, 'KeyFilename', Self.FKeyFilename);
   AnIni.WriteString(ASectionName, 'RemoteHost', Self.FRemoteHost);
   AnIni.WriteString(ASectionName, 'RemoteUsername', Self.FRemoteUsername);
   AnIni.WriteString(ASectionName, 'RemotePath', Self.FRemotePath);
   AnIni.WriteString(ASectionName, 'VolumeName', Self.FVolumeName);
end;

function TMacOSMountPoint.GetSSHFSParameters(AVolumesFolder: string): string;
var
   sOptions: string;
   sHost: string;
   sLocalPath: string;
begin
   sOptions := 'auto_cache';
   sOptions += ',cache_timeout=80000';
   sOptions += ',reconnect';
   sOptions += ',allow_other';
   sOptions += ',defer_permissions';
   sOptions += ',noappledouble';
   sOptions += ',IdentityFile=' + Self.FKeyFilename;
   sOptions += ',volname=' + Self.FVolumeName;
   sHost := Self.FRemoteUsername + '@' + Self.FRemoteHost + ':' + Self.FRemotePath;
   if Copy(Self.FFolderName, 1, 1) = '/' then begin
      sLocalPath := Self.FFolderName;
   end else begin
      sLocalPath := IncludeTrailingPathDelimiter(AVolumesFolder) + Self.FFolderName;
   end;
   Result := Format('-o %0:s %1:s %2:s', [sOptions, sHost, sLocalPath]);
end;

function TMacOSMountPoint.GetSSHFSParametersArray(AVolumesFolder: string): TParameterArray;
var
   sOptions: string;
   sHost: string;
   sLocalPath: string;
begin
   sOptions := 'auto_cache';
   sOptions += ',cache_timeout=80000';
   sOptions += ',reconnect';
   sOptions += ',allow_other';
   sOptions += ',defer_permissions';
   sOptions += ',noappledouble';
   sOptions += ',IdentityFile=' + Self.FKeyFilename;
   sOptions += ',volname=' + Self.FVolumeName;
   sHost := Self.FRemoteUsername + '@' + Self.FRemoteHost + ':' + Self.FRemotePath;
   if Copy(Self.FVolumeName, 1, 1) = '/' then begin
      sLocalPath := Self.FFolderName;
   end else begin
      sLocalPath := IncludeTrailingPathDelimiter(AVolumesFolder) + Self.FFolderName;
   end;
   Result := [];
   SetLength(Result, 4);
   Result[0] := '-o';
   Result[1] := sOptions;
   Result[2] := sHost;
   Result[3] := sLocalPath;
   //Result := Format('-o %0:s %1:s %2:s', [sOptions, sHost, sLocalPath]);
end;

function TMacOSMountPoint.IsMounted: boolean;
var
   sOut: string;
begin
   Result := False;
   if RunLoggedCommand('mount', [], sOut) then begin
      Result := Pos(Self.FolderName, sOut) > 0;
   end;
   // username@hostname:/var/www on /Users/xxx/Volumes/test (macfuse, nodev, nosuid, synchronous, mounted by xxx)
end;

procedure TMacOSMountPoint.Unmount(Sender: TObject);
begin
   RunUnMountCommand(Self.FolderName);
end;

procedure TMacOSMountPoint.Mount(Sender: TObject);
var
   sOut: string;
begin
   RunSSHFSCommand(Self.GetSSHFSParametersArray(), sOut);
end;

end.
