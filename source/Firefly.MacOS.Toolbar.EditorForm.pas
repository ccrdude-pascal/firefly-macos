{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Editor form for macOS toolbar items.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-02-07  pk  30m  Created draft of items editor form
// *****************************************************************************
   )
}

unit Firefly.MacOS.Toolbar.EditorForm;

{$mode Delphi}{$H+}
{$modeswitch objectivec1}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ButtonPanel,
   ComCtrls,
   StdCtrls,
   ActnList,
   ComboEx,
   Menus,
   ExtCtrls,
   StdActns,
   CocoaAll,
   Generics.Collections,
   Firefly.MacOS.ClassHelpers,
   Firefly.MacOS.Toolbar.Items;

type

   { TFormFireflyMacOSToolbarEditor }

   TFormFireflyMacOSToolbarEditor = class(TForm)
      aAddItem: TAction;
      aAddItemGroup: TAction;
      aAddMenuItem: TAction;
      aAddSearchItem: TAction;
      aFileNew: TAction;
      aDeleteItem: TAction;
      alEditor: TActionList;
      ButtonPanel1: TButtonPanel;
      cbIsSelectable: TCheckBox;
      cbIsDefault: TCheckBox;
      cbIsImmovable: TCheckBox;
      cbSystemImage: TComboBox;
      cbUseBundleFile: TComboBox;
      cbIsCentered: TCheckBox;
      cbImageList: TComboBoxEx;
      editTitle: TEdit;
      editHint: TEdit;
      editIdentifier: TEdit;
      aFileLoad: TFileOpen;
      aFileSave: TFileSaveAs;
      gbItem: TGroupBox;
      gbItemProperties: TGroupBox;
      ilSystemImages: TImageList;
      ilTree: TImageList;
      labelImage: TLabel;
      labelFlags: TLabel;
      labelTitle: TLabel;
      labelHint: TLabel;
      labelIdentifier: TLabel;
      MenuItem1: TMenuItem;
      MenuItem2: TMenuItem;
      MenuItem3: TMenuItem;
      MenuItem4: TMenuItem;
      MenuItem5: TMenuItem;
      MenuItem6: TMenuItem;
      MenuItem7: TMenuItem;
      panelBasic: TPanel;
      panelImage: TPanel;
      panelFlags: TPanel;
      popupAddItems: TPopupMenu;
      popupFile: TPopupMenu;
      rbUseImageList: TRadioButton;
      rbUseSystemImage: TRadioButton;
      rbUseBundleFile: TRadioButton;
      rbUseNoImage: TRadioButton;
      Separator2: TMenuItem;
      toolbarItems: TToolBar;
      tbAddItem: TToolButton;
      tbDeleteItem: TToolButton;
      tbList: TToolButton;
      tvItems: TTreeView;
      procedure aAddItemExecute({%H-}Sender: TObject);
      procedure aAddItemGroupExecute({%H-}Sender: TObject);
      procedure aAddMenuItemExecute({%H-}Sender: TObject);
      procedure aAddSearchItemExecute({%H-}Sender: TObject);
      procedure aDeleteItemExecute({%H-}Sender: TObject);
      procedure aFileLoadAccept({%H-}Sender: TObject);
      procedure aFileNewExecute({%H-}Sender: TObject);
      procedure aFileSaveAccept({%H-}Sender: TObject);
      procedure cbImageListChange({%H-}Sender: TObject);
      procedure cbIsCenteredChange(Sender: TObject);
      procedure cbIsDefaultChange(Sender: TObject);
      procedure cbIsImmovableChange(Sender: TObject);
      procedure cbIsSelectableChange(Sender: TObject);
      procedure editHintChange({%H-}Sender: TObject);
      procedure editIdentifierChange({%H-}Sender: TObject);
      procedure editTitleChange({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
      procedure imageSettingsChange({%H-}Sender: TObject);
      procedure labelIdentifierClick({%H-}Sender: TObject);
      procedure tvItemsSelectionChanged({%H-}Sender: TObject);
   private
      procedure AddItem(AClass: TMacToolbarItemClass; ADefaultIdentifier: string; ADefaultCaption: string);
   protected
      FItems: TMacToolbarItemList;
      procedure AssignToUI;
      procedure SelectItem(AnItem: TMacToolbarItem);
      procedure FillSystemImages;
      procedure FillImageList;
   public
      function EditItems(const TheItems: TMacToolbarItemList): boolean;
   end;

var
   FormFireflyMacOSToolbarEditor: TFormFireflyMacOSToolbarEditor;

function EditMacOSToolbarItems(const TheItems: TMacToolbarItemList): boolean;

implementation

{$R *.lfm}

uses
   // BGRABitmap,
   IntfGraphics,
   fpreadtiff;

type
   TSytemImageTemplate = class
      ImageName: NSString;
      ImageListIndex: integer;
   end;

   { TSytemImageTemplates }

   TSytemImageTemplates = class(TObjectList<TSytemImageTemplate>)
   public
      procedure Fill;
   end;


function EditMacOSToolbarItems(const TheItems: TMacToolbarItemList): boolean;
var
   form: TFormFireflyMacOSToolbarEditor;
begin
   form := TFormFireflyMacOSToolbarEditor.Create(nil);
   try
      Result := form.EditItems(TheItems);
   finally
      form.Free;
   end;
end;

{ TSytemImageTemplates }

procedure TSytemImageTemplates.Fill;

   procedure AddString(AString: NSString);
   var
      t: TSytemImageTemplate;
   begin
      t := TSytemImageTemplate.Create;
      t.ImageName := AString;
      Self.Add(t);
   end;

begin
   AddString(NSImageNameQuickLookTemplate);
   AddString(NSImageNameBluetoothTemplate);
   AddString(NSImageNameIChatTheaterTemplate);
   AddString(NSImageNameSlideshowTemplate);
   AddString(NSImageNameActionTemplate);
   AddString(NSImageNameSmartBadgeTemplate);
   AddString(NSImageNameIconViewTemplate);
   AddString(NSImageNameListViewTemplate);
   AddString(NSImageNameColumnViewTemplate);
   AddString(NSImageNameFlowViewTemplate);
   AddString(NSImageNamePathTemplate);
   AddString(NSImageNameInvalidDataFreestandingTemplate);
   AddString(NSImageNameLockLockedTemplate);
   AddString(NSImageNameLockUnlockedTemplate);
   AddString(NSImageNameGoRightTemplate);
   AddString(NSImageNameGoLeftTemplate);
   AddString(NSImageNameRightFacingTriangleTemplate);
   AddString(NSImageNameLeftFacingTriangleTemplate);
   AddString(NSImageNameAddTemplate);
   AddString(NSImageNameRemoveTemplate);
   AddString(NSImageNameRevealFreestandingTemplate);
   AddString(NSImageNameFollowLinkFreestandingTemplate);
   AddString(NSImageNameEnterFullScreenTemplate);
   AddString(NSImageNameExitFullScreenTemplate);
   AddString(NSImageNameStopProgressTemplate);
   AddString(NSImageNameStopProgressFreestandingTemplate);
   AddString(NSImageNameRefreshTemplate);
   AddString(NSImageNameRefreshFreestandingTemplate);
   AddString(NSImageNameBonjour);
   AddString(NSImageNameComputer);
   AddString(NSImageNameFolderBurnable);
   AddString(NSImageNameFolderSmart);
   AddString(NSImageNameFolder);
   AddString(NSImageNameNetwork);
   AddString(NSImageNameDotMac);
   AddString(NSImageNameMobileMe);
   AddString(NSImageNameMultipleDocuments);
   AddString(NSImageNameUserAccounts);
   AddString(NSImageNamePreferencesGeneral);
   AddString(NSImageNameAdvanced);
   AddString(NSImageNameInfo);
   AddString(NSImageNameFontPanel);
   AddString(NSImageNameColorPanel);
   AddString(NSImageNameUser);
   AddString(NSImageNameUserGroup);
   AddString(NSImageNameEveryone);
   AddString(NSImageNameUserGuest);
   AddString(NSImageNameMenuOnStateTemplate);
   AddString(NSImageNameMenuMixedStateTemplate);
   AddString(NSImageNameApplicationIcon);
   AddString(NSImageNameTrashEmpty);
   AddString(NSImageNameTrashFull);
   AddString(NSImageNameHomeTemplate);
   AddString(NSImageNameBookmarksTemplate);
   AddString(NSImageNameCaution);
   AddString(NSImageNameStatusAvailable);
   AddString(NSImageNameStatusPartiallyAvailable);
   AddString(NSImageNameStatusUnavailable);
   AddString(NSImageNameStatusNone);
   AddString(NSImageNameShareTemplate);

end;

{ TFormFireflyMacOSToolbarEditor }

procedure TFormFireflyMacOSToolbarEditor.tvItemsSelectionChanged(Sender: TObject);
var
   item: TMacToolbarItem;
   s: string = '';
   iItem: integer;
   iIndex: integer;
   cbe: TComboExItem;
begin
   if Assigned(tvItems.Selected) then begin
      aDeleteItem.Enabled := True;
      gbItemProperties.Enabled := True;
      item := tvItems.Selected.Data;
      editIdentifier.Caption := item.Identifier;
      editTitle.Caption := item.Caption;
      editHint.Caption := item.Hint;
      s.FromNSString(item.ImageDefaulType);
      cbSystemImage.ItemIndex := cbSystemImage.Items.IndexOf(s);
      if cbSystemImage.ItemIndex < 0 then begin
         cbSystemImage.ItemIndex := cbSystemImage.Items.Add(s);
      end;
      cbUseBundleFile.Text := item.ImageFilename;
      iIndex := 0;
      s := IntToStr(item.ImageIndex);
      for iItem := 0 to Pred(cbImageList.ItemsEx.Count) do begin
         cbe := cbImageList.ItemsEx[iItem];
         if (SameText(cbe.Caption, s)) then begin
            iIndex := cbe.Index;
         end;
      end;
      cbImageList.ItemIndex := iIndex;
      case item.ImageType of
         mtiitNone: begin
            rbUseNoImage.Checked := True;
         end;
         mtiitDefault: begin
            rbUseSystemImage.Checked := True;
         end;
         mtiitImageList: begin
            rbUseImageList.Checked := True;
         end;
         mtiitFile: begin
            rbUseBundleFile.Checked := True;
         end;
      end;
      cbIsCentered.Checked := item.IsCentered;
      cbIsDefault.Checked := item.IsDefault;
      cbIsImmovable.Checked := item.IsImmovable;
      cbIsSelectable.Checked := item.IsSelectable;
   end else begin
      aDeleteItem.Enabled := False;
      gbItemProperties.Enabled := False;
      editIdentifier.Caption := '';
      editTitle.Caption := '';
      editHint.Caption := '';
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.editTitleChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.Caption := editTitle.Text;
      if Length(editTitle.Text) > 0 then begin
         tvItems.Selected.Text := editTitle.Text;
      end else begin
         tvItems.Selected.Text := 'n/a';
      end;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.FormShow(Sender: TObject);
begin
   FillSystemImages;
end;

procedure TFormFireflyMacOSToolbarEditor.imageSettingsChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      gbItemProperties.Visible := True;
      item := tvItems.Selected.Data;
      item.ImageDefaulType := NSStr(cbSystemImage.Text);
      item.ImageFilename := cbUseBundleFile.Text;
      if rbUseSystemImage.Checked then begin
         item.ImageType := mtiitDefault;
      end else if rbUseBundleFile.Checked then begin
         item.ImageType := mtiitDefault;
      end else if rbUseImageList.Checked then begin
         item.ImageType := mtiitImageList;
      end else begin
         item.ImageType := mtiitNone;
      end;
      cbSystemImage.Visible := rbUseSystemImage.Checked;
      cbUseBundleFile.Visible := rbUseBundleFile.Checked;
      cbImageList.Visible := rbUseImageList.Checked;
   end else begin
      gbItemProperties.Visible := False;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.labelIdentifierClick(Sender: TObject);
begin

end;

procedure TFormFireflyMacOSToolbarEditor.editHintChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.Hint := editHint.Text;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.aAddItemGroupExecute(Sender: TObject);
begin
   AddItem(TMacToolbarItemGroup, 'NewItemGroup', 'New Item Group');
end;

procedure TFormFireflyMacOSToolbarEditor.aAddMenuItemExecute(Sender: TObject);
begin
   AddItem(TMacToolbarMenuItem, 'NewMenuItem', 'New Menu Item');
end;

procedure TFormFireflyMacOSToolbarEditor.aAddSearchItemExecute(Sender: TObject);
begin
   AddItem(TMacToolbarSearchItem, 'NewSearchItem', 'Search');
end;

procedure TFormFireflyMacOSToolbarEditor.AddItem(AClass: TMacToolbarItemClass; ADefaultIdentifier: string; ADefaultCaption: string);
var
   item: TMacToolbarItem;
   list: TMacToolbarItemList;
   tn: TTreeNode;
begin
   tn := tvItems.Selected;
   list := FItems;
   if Assigned(tn) then begin
      if (TMacToolbarItem(tn.Data) is TMacToolbarItemGroup) then begin
         list := TMacToolbarItemGroup(tn.Data).SubItems;
      end;
      if (TMacToolbarItem(tn.Data) is TMacToolbarItemGroup) then begin
         list := TMacToolbarItem(tn.Data).Owner;
         item := list.AddItem(AClass);
      end else begin
         list := TMacToolbarItem(tn.Data).Owner;
         item := list.AddItemAfter(TMacToolbarItem(tn.Data));
      end;
   end else begin
      item := FItems.AddItem(AClass);
   end;
   item.Identifier := ADefaultIdentifier + FormatDateTime('hhnnsszzz', Now);
   item.Caption := ADefaultCaption;
   AssignToUI;
   SelectItem(item);
end;

procedure TFormFireflyMacOSToolbarEditor.aAddItemExecute(Sender: TObject);
begin
   AddItem(TMacToolbarItem, 'NewItem', 'New Item');
end;

procedure TFormFireflyMacOSToolbarEditor.aDeleteItemExecute(Sender: TObject);
var
   tn: TTreeNode;
begin
   tn := tvItems.Selected;
   if not Assigned(tn) then begin
      Exit;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.aFileLoadAccept(Sender: TObject);
begin

end;

procedure TFormFireflyMacOSToolbarEditor.aFileNewExecute(Sender: TObject);
begin
   FItems.Clear;
   AssignToUI;
end;

procedure TFormFireflyMacOSToolbarEditor.aFileSaveAccept(Sender: TObject);
begin
   //FItems.Save11
end;

procedure TFormFireflyMacOSToolbarEditor.cbImageListChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) and (cbImageList.ItemIndex > -1) then begin
      item := tvItems.Selected.Data;
      item.ImageIndex := StrToIntDef(cbImageList.ItemsEx[cbImageList.ItemIndex].Caption, -1);
      tvItems.Selected.ImageIndex := item.ImageIndex;
      tvItems.Selected.SelectedIndex := item.ImageIndex;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.cbIsCenteredChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.IsCentered := (Sender as TCheckBox).Checked;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.cbIsDefaultChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.IsImmovable := (Sender as TCheckBox).Checked;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.cbIsImmovableChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.IsImmovable := (Sender as TCheckBox).Checked;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.cbIsSelectableChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.IsSelectable := (Sender as TCheckBox).Checked;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.editIdentifierChange(Sender: TObject);
var
   item: TMacToolbarItem;
begin
   if Assigned(tvItems.Selected) then begin
      item := tvItems.Selected.Data;
      item.Identifier := editIdentifier.Text;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.AssignToUI;

   procedure ItemsToTree(TheItems: TMacToolbarItemList; TheParentNode: TTreeNode);
   var
      item: TMacToolbarItem;
      s: string;
      n: TTreeNode;
   begin
      for item in TheItems do begin
         s := item.Caption;
         if (0 = Length(s)) then begin
            s := 'n/a';
         end;
         n := tvItems.Items.AddChild(TheParentNode, s);
         n.Data := item;
         //n.ImageIndex := item.ImageIndex;
         //n.SelectedIndex := item.ImageIndex;
         if (item is TMacToolbarMenuItem) then begin
            n.ImageIndex := 3;
            n.SelectedIndex := 3;
         end else if (item is TMacToolbarSearchItem) then begin
            n.ImageIndex := 2;
            n.SelectedIndex := 2;
         end else if (item is TMacToolbarItemGroup) then begin
            n.ImageIndex := 1;
            n.SelectedIndex := 1;
         end else begin
            n.ImageIndex := 0;
            n.SelectedIndex := 0;
         end;
         ItemsToTree(item.SubItems, n);
      end;
   end;

begin
   tvItems.BeginUpdate;
   try
      FillImageList;
      tvItems.Items.Clear;
      // tvItems.Images := FItems.ImageList;
      ItemsToTree(FItems, nil);
   finally
      tvItems.FullExpand;
      tvItems.EndUpdate;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.SelectItem(AnItem: TMacToolbarItem);
var
   n: TTreeNode;
begin
   for n in tvItems.Items do begin
      if n.Data = AnItem then begin
         tvItems.Selected := n;
         Exit;
      end;
   end;
end;

procedure TFormFireflyMacOSToolbarEditor.FillSystemImages;

   procedure AddString(AString: NSString);
   var
      s: string = '';
   begin
      s.FromNSString(AString);
      cbSystemImage.Items.Add(s);
   end;

   procedure LoadImage(ATemplate: TSytemImageTemplate);
   var
      nImage: NSImage;
      nData: NSData;
      ms: TMemoryStream;
      (* fails
      bgra: TBGRABitmap;
      *)
      (* fails, @see https://forum.lazarus.freepascal.org/index.php?topic=32356.0
      tfBitmap: TTiffImage;
      bmBitmap: TBitmap;
      *)
      (* fails, @see https://forum.lazarus.freepascal.org/index.php?topic=7926.0
      ImgReader: TFPReaderTiff;
      IntfImg: TLazIntfImage;
      *)
   begin
      nImage := NSImage.imageNamed(ATemplate.ImageName);
      nData := nImage.TIFFRepresentation;
      ms := TMemoryStream.Create;
      try
         ms.Write(nData.Bytes^, nData.Length);
         ms.Seek(0, soFromBeginning);
         (*
         bgra := TBGRABitmap.Create(ms);
         bgra.SaveToFile('/Users/patrick/toolbar-' + IntToStr(ATemplate.ImageListIndex) + 'bgra1.tiff');
         *)
         (* fails, @see https://forum.lazarus.freepascal.org/index.php?topic=32356.0
         tfBitmap := TTiffImage.Create;
         try
            tfBitmap.LoadFromStream(ms);
            tfBitmap.SaveToFile('/Users/patrick/toolbar-' + IntToStr(ATemplate.ImageListIndex) + '.tiff');
            bmBitmap := TBitmap.Create;
            try
               bmBitmap.Assign(tfBitmap);
               ATemplate.ImageListIndex := Self.ilSystemImages.Add(bmBitmap, nil);
               if (0 = ATemplate.ImageListIndex) then begin
                  ms.SaveToFile('/Users/patrick/toolbar-' + IntToStr(ATemplate.ImageListIndex) + '.stream.tiff');
                  nData.writeToFile_atomically(NSStr('/Users/patrick/toolbar-' + IntToStr(ATemplate.ImageListIndex) + '-nsdata.tiff'), false);
                  bmBitmap.SaveToFile('/Users/patrick/toolbar-' + IntToStr(ATemplate.ImageListIndex) + '.bmp');
               end;
            except
               bmBitmap.Free;
            end;
         finally
            tfBitmap.Free;
            ms.Free;
         end;
         *)
         (* fails, @see https://forum.lazarus.freepascal.org/index.php?topic=7926.0
         IntfImg := TLazIntfImage.Create(0,0,[]);
         IntfImg.DataDescription := GetDescriptionFromDevice(0, 0, 0);
         ImgReader := TFPReaderTiff.Create;
         IntfImg.LoadFromStream(ms, ImgReader);
         bmBitmap := TBitmap.Create;
            try
               bmBitmap.LoadFromIntfImage(IntfImg);
               bmBitmap.SaveToFile('/Users/patrick/toolbar-' + IntToStr(ATemplate.ImageListIndex) + '-intf.bmp');
            except
               bmBitmap.Free;
            end;
         *)
      finally
         ms.Free;
      end;
   end;

var
   list: TSytemImageTemplates;
   t: TSytemImageTemplate;
begin
   cbSystemImage.Items.BeginUpdate;
   try
      cbSystemImage.Items.Clear;

      list := TSytemImageTemplates.Create;
      try
         list.Fill;
         for t in list do begin
            AddString(t.ImageName);
         end;
      finally
         list.Free;
      end;
   finally
      cbSystemImage.Items.EndUpdate;
   end;

end;

procedure TFormFireflyMacOSToolbarEditor.FillImageList;
var
   item: TComboExItem;
   i: integer;
begin
   cbImageList.ItemsEx.BeginUpdate;
   try
      cbImageList.ItemsEx.Clear;
      if not Assigned(FItems.ImageList) then begin
         Exit;
      end;
      cbImageList.Images := FItems.ImageList;
      if (0 = FItems.ImageList.Count) then begin
         Exit;
      end;
      item := cbImageList.ItemsEx.Add;
      item.ImageIndex := -1;
      item.Caption := '-1';
      for i := 0 to PRed(FItems.ImageList.Count) do begin
         item := cbImageList.ItemsEx.Add;
         item.ImageIndex := i;
         item.Caption := IntToStr(i);
      end;
   finally
      cbImageList.ItemsEx.EndUpdate;
   end;
end;

function TFormFireflyMacOSToolbarEditor.EditItems(const TheItems: TMacToolbarItemList): boolean;
begin
   FItems := TMacToolbarItemList.Create();
   FItems.Assign(TheItems);
   try
      AssignToUI;
      Result := (mrOk = ShowModal);
      if Result then begin
         TheItems.Assign(FItems);
      end;
   finally
      FItems.Free;
   end;
end;

end.
